"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const cors_1 = __importDefault(require("cors"));
const userRouter_1 = __importDefault(require("./router/userRouter"));
const profileRouter_1 = __importDefault(require("./router/profileRouter"));
const postRouter_1 = __importDefault(require("./router/postRouter"));
const app = (0, express_1.default)();
dotenv_1.default.config({
    path: './.env'
});
// express to read form data
app.use(express_1.default.json());
// configure cors
app.use((0, cors_1.default)());
// connect to DB
const dbName = process.env.DATABASE_NAME;
const dbUrl = process.env.MONGO_DB_URL;
// connect to Mongo DB
if (dbName && dbUrl) {
    // DBUtil.connectToDB(dbUrl, dbName).then((response) => {
    //     console.log(response);
    // }).catch((error) => {
    //     console.error(error);
    // });
}
const port = Number(process.env.PORT) || 9999;
app.get('/', (request, response) => {
    response.status(200).json({
        msg: 'Welcome to React Social App from Express!'
    });
});
// router configuration
app.use('/api/users', userRouter_1.default);
app.use('/api/profiles', profileRouter_1.default);
app.use('/api/posts', postRouter_1.default);
if (port) {
    app.listen(port, () => {
        console.log(`Express Server is started at123 : ${port}`);
    });
}
