import {Router, Request, Response} from 'express';
import {body, validationResult} from "express-validator";
import bcrypt from 'bcryptjs';
import UserTable from "../database/userSchema";
import gravatar from 'gravatar';
import jwt from 'jsonwebtoken';
import tokenVerifier from "../middleware/TokenVerifier";
import constants from '../constants.json';
import {IUser} from "../models/IUser";
import { DBUtil } from '../util/DBUtil';

const mysql = require('mysql');

const userRouter: Router = Router();

/*
    @sno : 1
    @usage : Register a User
    @url : http://127.0.0.1:9999/api/users/register
    @fields : name , email, password
    @method : POST
    @access : PUBLIC
 */
userRouter.post('/register', [
    body('name').not().isEmpty().withMessage('Name is Required'),
    body('email').isEmail().withMessage('Proper Email is Required'),
    body('password').isStrongPassword({
        minLength: 5,
        minLowercase: 1,
        minUppercase: 1,
        minNumbers: 1,
        minSymbols: 1
    }).withMessage('Strong Password is Required'),
], async (request: Request, response: Response) => {

    // validate the form from client
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({errors: errors.array()});
    }
     let {name, email, password} = request.body;
        
       
       
        try {
            console.log('Before connecting to the database');
           
             const connection = await DBUtil.connectToDB()
             let salt = await bcrypt.genSalt(10);
             let hashPassword = await bcrypt.hash(password, salt);
     
            const checkQuery = 'SELECT * FROM users WHERE email = ?';
            connection.query(checkQuery, [ email], (checkError:any, checkResults:any) => {
                if (checkError) {
                    console.error('Error checking for existing user:', checkError);
                    connection.end(); // Close the connection
                     return;
                }
 
                if (checkResults.length > 0) {
                // A user with the same email already exists
                    console.error('User with this email already exists');
                    connection.end(); // Close the connection
                    response.status(500).json({
                        msg: 'User already registered.'
                    });
                    return;
                     
                }
                 // encrypt password
        
        // get gravatar url
        let imageUrl = gravatar.url(email, {
            s: '200',
            r: 'pg',
            d: 'mm'
        });
 
        // register a user
        let userData: IUser = {
            name: name,
            email: email,
            password: hashPassword,
            avatar: imageUrl,
            isAdmin: false
        };
                const query = `
              INSERT INTO users (name, email, password, avatar, isAdmin)
              VALUES (?, ?, ?, ?, ?)
            `;
            const values = [userData.name, userData.email, userData.password, userData.avatar, userData.isAdmin];
          
            console.log('Before executing the query');
            connection.query(query, values, (error: any, results: any) => {
                console.log('Inside query callback');
              if (error) {
                console.error('Error inserting data:', error);
                response.status(500).json({
                  msg: 'User Registration failed'
                });
              } else {
                console.log('Data inserted successfully.');
                response.status(200).json({
                  msg: 'User Registration is success!'
                });
              }
              console.log('After executing the query');
          
              // Close the connection
              connection.end((error: any) => {
                if (error) {
                  console.error('Error closing the database connection:', error);
                }
                console.log('Disconnected from the MySQL database');
              });
            });
        })
  
          } catch (error) {
            console.error('Error in try-catch:', error);
            response.status(500).json({
              msg: 'Server Error'
            });
          }

});

/*
    @sno : 2
    @usage : Login a User
    @url : http://127.0.0.1:9999/api/users/login
    @fields : email, password
    @method : POST
    @access : PUBLIC
 */
userRouter.post('/login', [
    body('email').isEmail().withMessage('Proper Email is Required'),
    body('password').isStrongPassword({
        minLength: 5,
        minLowercase: 1,
        minUppercase: 1,
        minNumbers: 1,
        minSymbols: 1
    }).withMessage('Strong Password is Required'),
], async (request: Request, response: Response) => {

    // validate the form from client
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({errors: errors.array()});
    }

    try {
        let {email, password} = request.body;

        // check if email is exists

        const connection = await DBUtil.connectToDB()

       const checkQuery = 'SELECT *  FROM users WHERE email = ?';

       const resultdata = await queryDatabase(connection, checkQuery, [email]);
console.log("resultdata: ", resultdata)
       if (resultdata && Array.isArray(resultdata) && resultdata.length > 0) {
        // The results object exists, is an array, and contains at least one record
        const userRecord = resultdata[0]; // Assuming it's the first record
        const userId = userRecord.user_id;
        console.log('User ID:', userId);
        let isMatch = await bcrypt.compare(password, userRecord.password);
            if (!isMatch) {
                return response.status(401).json({
                    msg: 'Invalid Credentials Password!'
                });
            }

            const payload: any = {
                          user: {
                              id: userId,
                              email: userRecord.email
                         }
                      };

            const secretKey: string | undefined = process.env.JWT_SECRET_KEY;
            if (secretKey) {
                jwt.sign(payload, secretKey, {expiresIn: 10000000}, (err, encoded) => {
                    return response.status(200).json({
                        msg: 'Login user is Success',
                        token: encoded,
                        user: {
                            "user_id": userRecord.user_id,
                            "name": userRecord.name,
                            "email": userRecord.email,
                             "isAdmin": userRecord.isAdmin,
                            "avatar": userRecord.avatar
                        }
                    })
                })
            }
        
      }  else {
        return response.status(401).json({
                    msg: 'Invalid Credentials Email!'
                       });
      }
    
    } catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
});

/*
   @sno : 3
   @usage : Get User Info
   @url : http://127.0.0.1:9999/api/users/me
   @fields : no-fields
   @method : GET
   @access : PRIVATE
*/
userRouter.get('/me', tokenVerifier, async (request: Request, response: Response) => {
    try {
         let user: any = request.headers[constants.APP_CONSTANTS.USER];

        const connection = await DBUtil.connectToDB()

        const userQuery = 'SELECT  user_id,name,email,isAdmin,avatar  FROM users WHERE user_id = ?';
        console.log('User ID:', user.id);
        const resultdata = await queryDatabase(connection, userQuery, [user.id]);
        if (resultdata && Array.isArray(resultdata) && resultdata.length > 0) {
            // The results object exists, is an array, and contains at least one record
            const userRecord = resultdata[0]; // Assuming it's the first record
            const userId = userRecord.user_id;
            console.log('User ID:', userId);
            response.status(200).json({
                user: userRecord
            })
        } else {
            return response.status(404).json({
                msg: 'User not found!'
            });
        }

         
    } catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
});




function queryDatabase(connection:any, sql:any, values:any) {
    return new Promise((resolve, reject) => {
      connection.query(sql, values, (error:any, results:any) => {
        if (error) {
          reject(error);
        } else {
          resolve(results);
        }
      });
    });
  }

export default userRouter;