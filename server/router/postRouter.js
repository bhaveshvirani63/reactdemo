"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const express_validator_1 = require("express-validator");
const TokenVerifier_1 = __importDefault(require("../middleware/TokenVerifier"));
const constants_json_1 = __importDefault(require("../constants.json"));
const mongoose_1 = __importDefault(require("mongoose"));
const DBUtil_1 = require("../util/DBUtil");
// const multer = require('multer');
const multer_1 = __importDefault(require("multer"));
const postRouter = (0, express_1.Router)();
const path_1 = __importDefault(require("path")); // Import the 'path' module
const fs_1 = __importDefault(require("fs")); // Import the 'fs' module for file system operations
const storage = multer_1.default.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/'); // Specify the directory where files will be stored
    },
    filename: (req, file, cb) => {
        const extname = path_1.default.extname(file.originalname);
        cb(null, `${Date.now()}${extname}`); // Rename the file to avoid overwriting
    },
});
const uploadDirectory = 'uploads';
const upload = (0, multer_1.default)({ storage });
if (!fs_1.default.existsSync(uploadDirectory)) {
    fs_1.default.mkdirSync(uploadDirectory);
}
/*
    @sno : 1
    @usage : Create a Post
    @url : http://127.0.0.1:9999/api/posts/
    @fields : image , text
    @method : POST
    @access : PRIVATE
 */
// postRouter.post('/upload', upload.single('image'), (req: Request, res: Response) => {
postRouter.post('/', [
// body('image').not().isEmpty().withMessage('Image is Required'),
// body('text').not().isEmpty().withMessage('Text is Required'),
], upload.single('imageFile'), TokenVerifier_1.default, (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    // validate the form from client
    console.log('f=======================');
    console.log('f=======================', request);
    const errors = (0, express_validator_1.validationResult)(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({ errors: errors.array() });
    }
    try {
        if (request.file) {
            console.log('file uploaded success sdcssdsdds',request.file.filename);
        }
        let { text, image } = request.body;
        let requestedUser = request.headers[constants_json_1.default.APP_CONSTANTS.USER];
        const connection = yield DBUtil_1.DBUtil.connectToDB();
        const userQuery = 'SELECT  user_id,name,email,isAdmin,avatar  FROM users WHERE user_id = ?';
        console.log('User ID:', requestedUser.id);
        console.log('User ID Vbah:', requestedUser.id);
        const resultdata = yield queryDatabase(connection, userQuery, [requestedUser.id]);
        if (resultdata && Array.isArray(resultdata) && resultdata.length > 0) {
            // The results object exists, is an array, and contains at least one record
            const userRecord = resultdata[0]; // Assuming it's the first record
            const userId = userRecord.user_id;
            console.log('User ID:', userId);
            let postObj = {
                user_id: requestedUser.id,
                text: text,
                image:  request?.file?.filename || "",
                name: userRecord.name,
                avatar: userRecord.avatar,
                likes: [],
                comments: [],
            };
            const query = `
            INSERT INTO posts (user_id ,text, image, name, avatar)
            VALUES (?,?, ?, ?, ?)
          `;
            const values = [postObj.user_id, postObj.text, postObj.image, postObj.name,
                postObj.avatar];
            const insertedEducation = yield queryDatabase(connection, query, values);
            return response.status(200).json({
                msg: 'Post is created!',
                post: postObj
            });
        }
        else {
            return response.status(404).json({
                msg: 'User not found!'
            });
        }
    }
    catch (error) {
        console.log(error);
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
}));
/*
    @sno : 2
    @usage : Get All the posts
    @url : http://127.0.0.1:9999/api/posts/
    @fields : no-fields
    @method : GET
    @access : PRIVATE
 */
postRouter.get('/', TokenVerifier_1.default, (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const connection = yield DBUtil_1.DBUtil.connectToDB();
        const userQuery = `SELECT  *  FROM posts `;
        const postData = yield queryDatabase(connection, userQuery, []);
        var allPosts = [];
        if (postData && Array.isArray(postData) && postData.length > 0) {
            for (let index = 0; index < postData.length; index++) {
                const element = postData[index];
                const postLikeQuery = `SELECT  *  FROM post_like where post_id = ${element.post_id}`;
                const postLikeData = yield queryDatabase(connection, postLikeQuery, []);
                const post = yield creatPostObject(element.post_id);
                allPosts.push(post);
            }
            return response.status(200).json({
                posts: allPosts
            });
        }
    }
    catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
}));
/*
    @sno : 3
    @usage : Get a post with Post Id
    @url : http://127.0.0.1:9999/api/posts/:postId
    @fields : no-fields
    @method : GET
    @access : PRIVATE
 */
postRouter.get('/:postId', TokenVerifier_1.default, (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { postId } = request.params;
        const connection = yield DBUtil_1.DBUtil.connectToDB();
        const userQuery = `SELECT  *  FROM posts where post_id = ${postId} `;
        const resultdata = yield queryDatabase(connection, userQuery, []);
        if (resultdata && Array.isArray(resultdata) && resultdata.length > 0) {
            return response.status(200).json({
                post: resultdata
            });
        }
        else {
            return response.status(404).json({
                errors: [
                    {
                        msg: 'No Posts Found!'
                    }
                ]
            });
        }
    }
    catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
}));
/*
    @sno : 4
    @usage : Delete a post with Post Id
    @url : http://127.0.0.1:9999/api/posts/:postId
    @fields : no-fields
    @method : DELETE
    @access : PRIVATE
 */
postRouter.delete('/:postId', TokenVerifier_1.default, (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { postId } = request.params;
        const mongoPostId = new mongoose_1.default.Types.ObjectId(postId);
        const connection = yield DBUtil_1.DBUtil.connectToDB();
        const userQuery = `SELECT  *  FROM posts where post_id = ${postId} `;
        const resultdata = yield queryDatabase(connection, userQuery, []);
        if (resultdata && Array.isArray(resultdata) && resultdata.length == 0) {
            return response.status(404).json({
                errors: [
                    {
                        msg: 'No Posts Found!'
                    }
                ]
            });
        }
        const deleteQuery = `DELETE posts where post_id = ${postId} `;
        const deletedData = yield queryDatabase(connection, deleteQuery, []);
        return response.status(200).json({
            msg: 'Post is Removed!',
            post: resultdata
        });
    }
    catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
}));
/*
    @sno : 5
    @usage : Like A Post with PostId
    @url : http://127.0.0.1:9999/api/posts/like/:postId
    @fields : no-fields
    @method : POST
    @access : PRIVATE
 */
postRouter.post('/like/:postId', TokenVerifier_1.default, (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { postId } = request.params;
        let requestedUser = request.headers[constants_json_1.default.APP_CONSTANTS.USER];
        const connection = yield DBUtil_1.DBUtil.connectToDB();
        const postSelectQuery = `SELECT  *  FROM posts where post_id = ${postId} `;
        const postData = yield queryDatabase(connection, postSelectQuery, []);
        if (postData && Array.isArray(postData) && postData.length == 0) {
            return response.status(400).json({
                errors: [
                    {
                        msg: 'No Posts Found for the Post ID'
                    }
                ]
            });
        }
        else {
            const postLikeQuery = `SELECT  *  FROM post_like where post_id = ${postId} AND user_id = ${requestedUser.id} `;
            console.log('postLikeQuery: ', postLikeQuery);
            const postLikeData = yield queryDatabase(connection, postLikeQuery, []);
            if (postLikeData && Array.isArray(postLikeData) && postLikeData.length > 0) {
                return response.status(400).json({
                    errors: [
                        {
                            msg: 'Post has already been liked'
                        }
                    ]
                });
            }
            else {
                const query = `
                INSERT INTO post_like (user_id ,post_id)
                VALUES (?,?)
              `;
                const values = [requestedUser.id, postId];
                const insertedEducation = yield queryDatabase(connection, query, values);
                response.status(200).json({
                    post: yield creatPostObject(postId)
                });
            }
        }
    }
    catch (error) {
        response.status(500).json({
            errors: [
                {
                    msg: error
                }
            ]
        });
    }
}));
/*
    @sno : 6
    @usage : Un-Like A Post with PostId
    @url : http://127.0.0.1:9999/api/posts/unlike/:postId
    @fields : no-fields
    @method : PUT
    @access : PRIVATE
 */
postRouter.put('/unlike/:postId', TokenVerifier_1.default, (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { postId } = request.params;
        let requestedUser = request.headers[constants_json_1.default.APP_CONSTANTS.USER];
        const connection = yield DBUtil_1.DBUtil.connectToDB();
        const userQuery = `SELECT  *  FROM posts where post_id = ${postId} `;
        const postData = yield queryDatabase(connection, userQuery, []);
        if (postData && Array.isArray(postData) && postData.length == 0) {
            return response.status(400).json({
                errors: [
                    {
                        msg: 'No Posts Found for the Post ID'
                    }
                ]
            });
        }
        else {
            const postLikeQuery = `SELECT  *  FROM post_like where post_id = ${postId} AND user_id = ${requestedUser.id} `;
            console.log('postLikeQuery: ', postLikeQuery);
            const postLikeData = yield queryDatabase(connection, postLikeQuery, []);
            if (postLikeData && Array.isArray(postLikeData) && postLikeData.length == 0) {
                return response.status(400).json({
                    errors: [
                        {
                            msg: 'Post has not been liked'
                        }
                    ]
                });
            }
            else {
                const deleteQuery = `DELETE FROM post_like where post_id = ${postId} AND user_id = ${requestedUser.id}  `;
                const deletedData = yield queryDatabase(connection, deleteQuery, []);
                response.status(200).json({
                    post: yield creatPostObject(postId)
                });
            }
        }
    }
    catch (error) {
        console.log("Error:", error);
        response.status(500).json({
            errors: [
                {
                    msg: error
                }
            ]
        });
    }
}));
/*
    @sno : 7
    @usage : Create Comment to a post
    @url : http://127.0.0.1:9999/api/posts/comment/:postId
    @fields : text
    @method : POST
    @access : PRIVATE
 */
postRouter.post('/comment/:postId', [
    (0, express_validator_1.body)('text').not().isEmpty().withMessage('Text is Required'),
], TokenVerifier_1.default, (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    const errors = (0, express_validator_1.validationResult)(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({ errors: errors.array() });
    }
    try {
        let { text } = request.body;
        let { postId } = request.params;
        let requestedUser = request.headers[constants_json_1.default.APP_CONSTANTS.USER];
        const connection = yield DBUtil_1.DBUtil.connectToDB();
        const postSelectQuery = `SELECT  *  FROM posts where post_id = ${postId} `;
        const postData = yield queryDatabase(connection, postSelectQuery, []);
        if (postData && Array.isArray(postData) && postData.length == 0) {
            return response.status(400).json({
                errors: [
                    {
                        msg: 'No Posts Found for the Post ID'
                    }
                ]
            });
        }
        else {
            const userSelectQuery = `SELECT  *  FROM users where user_id = ${requestedUser.id} `;
            const userData = yield queryDatabase(connection, userSelectQuery, []);
            if (userData && Array.isArray(userData) && userData.length > 0) {
                const userRecord = userData[0];
                const commentInsertQuery = `
                INSERT INTO post_comment (user_id ,post_id, text , name, avatar)
                VALUES (?,?,?,?,?)
              `;
                const commentValues = [requestedUser.id, postId, text, userRecord.name, userRecord.avatar];
                const insertedComment = yield queryDatabase(connection, commentInsertQuery, commentValues);
                console.log('insertedComment', insertedComment);
                response.status(200).json({
                    msg: 'Comment is Created Successfully',
                    post: yield creatPostObject(postId)
                });
            }
        }
    }
    catch (error) {
        response.status(500).json({
            errors: [
                {
                    msg: error
                }
            ]
        });
    }
}));
/*
    @sno : 8
    @usage : DELETE Comment of a post
    @url : http://127.0.0.1:9999/api/posts/comment/:postId/:commentId
    @fields : no-fields
    @method : DELETE
    @access : PRIVATE
 */
postRouter.delete('/comment/:postId/:commentId', TokenVerifier_1.default, (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let { postId, commentId } = request.params;
        let mongoPostID = new mongoose_1.default.Types.ObjectId(postId);
        //let mongoCommentID = new mongoose.Types.ObjectId(commentId);
        let requestedUser = request.headers[constants_json_1.default.APP_CONSTANTS.USER];
        const connection = yield DBUtil_1.DBUtil.connectToDB();
        const selectCommentQuery = `SELECT  *  FROM post_comment where post_id = ${postId} AND comment_id =${commentId} `;
        const commentData = yield queryDatabase(connection, selectCommentQuery, []);
        if (commentData && Array.isArray(commentData) && commentData.length == 0) {
            return response.status(400).json({
                errors: [
                    {
                        msg: 'Comment not exists'
                    }
                ]
            });
        }
        else {
            if (commentData && Array.isArray(commentData) && commentData.length > 0) {
                const commentRecord = commentData[0];
                if (commentRecord.user_id.toString() !== requestedUser.id) {
                    return response.status(401).json({
                        errors: [
                            {
                                msg: 'User is not authorized'
                            }
                        ]
                    });
                }
                const deleteQuery = `DELETE FROM post_comment where comment_id  = ${commentId}    `;
                const deletedData = yield queryDatabase(connection, deleteQuery, []);
                response.status(200).json({
                    msg: 'Comment is Deleted',
                    post: yield creatPostObject(postId)
                });
            }
        }
    }
    catch (error) {
        response.status(500).json({
            errors: [
                {
                    msg: error
                }
            ]
        });
    }
}));
function creatPostObject(postId) {
    return __awaiter(this, void 0, void 0, function* () {
        const connection = yield DBUtil_1.DBUtil.connectToDB();
        const postSelectQuery = `SELECT  *  FROM posts where post_id = ${postId} `;
        const postData = yield queryDatabase(connection, postSelectQuery, []);
        if (postData && Array.isArray(postData) && postData.length > 0) {
            var postRecord = postData[0];
            var post = {
                user_id: postRecord.user,
                text: postRecord.text,
                image: postRecord.image,
                name: postRecord.name,
                avatar: postRecord.avatar,
                likes: [],
                comments: []
            };
            console.log('postRecord:123 ', postRecord);
            const postLikeQuery = `SELECT  *  FROM post_like where post_id = ${postId}`;
            const postLikeData = yield queryDatabase(connection, postLikeQuery, []);
            const postCommentQuery = `SELECT  *  FROM post_comment where post_id = ${postId}`;
            const postCommentData = yield queryDatabase(connection, postCommentQuery, []);
            post = {
                user_id: postRecord.user_id,
                post_id: postId,
                text: postRecord.text,
                image: postRecord.image,
                name: postRecord.name,
                avatar: postRecord.avatar,
                likes: postLikeData,
                comments: postCommentData
            };
            return post;
        }
        return {};
    });
}
function queryDatabase(connection, sql, values) {
    return new Promise((resolve, reject) => {
        connection.query(sql, values, (error, results) => {
            if (error) {
                console.log('error: ', error);
                reject(error);
            }
            else {
                console.log("Number of records inserted: " + results.toString());
                console.log("Number of records inserted: " + results.insertId);
                resolve(results);
            }
        });
    });
}
postRouter.post('/upload', upload.single('image'), (req, res) => {
    // The 'image' parameter is the name of the input field in your form
    // You can access the uploaded file information using req.file
    if (!req.file) {
        return res.status(400).send('No file uploaded.');
    }
    // You can process the uploaded file here and store it in your database or file system
    res.status(200).send('File uploaded successfully.');
});
exports.default = postRouter;
