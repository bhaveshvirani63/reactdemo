"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const express_validator_1 = require("express-validator");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const gravatar_1 = __importDefault(require("gravatar"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const TokenVerifier_1 = __importDefault(require("../middleware/TokenVerifier"));
const constants_json_1 = __importDefault(require("../constants.json"));
const DBUtil_1 = require("../util/DBUtil");
const mysql = require('mysql');
const userRouter = (0, express_1.Router)();
/*
    @sno : 1
    @usage : Register a User
    @url : http://127.0.0.1:9999/api/users/register
    @fields : name , email, password
    @method : POST
    @access : PUBLIC
 */
userRouter.post('/register', [
    (0, express_validator_1.body)('name').not().isEmpty().withMessage('Name is Required'),
    (0, express_validator_1.body)('email').isEmail().withMessage('Proper Email is Required'),
    (0, express_validator_1.body)('password').isStrongPassword({
        minLength: 5,
        minLowercase: 1,
        minUppercase: 1,
        minNumbers: 1,
        minSymbols: 1
    }).withMessage('Strong Password is Required'),
], (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    // validate the form from client
    const errors = (0, express_validator_1.validationResult)(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({ errors: errors.array() });
    }
    let { name, email, password } = request.body;
    try {
        console.log('Before connecting to the database');
        const connection = yield DBUtil_1.DBUtil.connectToDB();
        let salt = yield bcryptjs_1.default.genSalt(10);
        let hashPassword = yield bcryptjs_1.default.hash(password, salt);
        const checkQuery = 'SELECT * FROM users WHERE email = ?';
        connection.query(checkQuery, [email], (checkError, checkResults) => {
            if (checkError) {
                console.error('Error checking for existing user:', checkError);
                connection.end(); // Close the connection
                return;
            }
            if (checkResults.length > 0) {
                // A user with the same email already exists
                console.error('User with this email already exists');
                connection.end(); // Close the connection
                response.status(500).json({
                    msg: 'User already registered.'
                });
                return;
            }
            // encrypt password
            // get gravatar url
            let imageUrl = gravatar_1.default.url(email, {
                s: '200',
                r: 'pg',
                d: 'mm'
            });
            // register a user
            let userData = {
                name: name,
                email: email,
                password: hashPassword,
                avatar: imageUrl,
                isAdmin: false
            };
            const query = `
              INSERT INTO users (name, email, password, avatar, isAdmin)
              VALUES (?, ?, ?, ?, ?)
            `;
            const values = [userData.name, userData.email, userData.password, userData.avatar, userData.isAdmin];
            console.log('Before executing the query');
            connection.query(query, values, (error, results) => {
                console.log('Inside query callback');
                if (error) {
                    console.error('Error inserting data:', error);
                    response.status(500).json({
                        msg: 'User Registration failed'
                    });
                }
                else {
                    console.log('Data inserted successfully.');
                    response.status(200).json({
                        msg: 'User Registration is success!'
                    });
                }
                console.log('After executing the query');
                // Close the connection
                connection.end((error) => {
                    if (error) {
                        console.error('Error closing the database connection:', error);
                    }
                    console.log('Disconnected from the MySQL database');
                });
            });
        });
    }
    catch (error) {
        console.error('Error in try-catch:', error);
        response.status(500).json({
            msg: 'Server Error'
        });
    }
}));
/*
    @sno : 2
    @usage : Login a User
    @url : http://127.0.0.1:9999/api/users/login
    @fields : email, password
    @method : POST
    @access : PUBLIC
 */
userRouter.post('/login', [
    (0, express_validator_1.body)('email').isEmail().withMessage('Proper Email is Required'),
    (0, express_validator_1.body)('password').isStrongPassword({
        minLength: 5,
        minLowercase: 1,
        minUppercase: 1,
        minNumbers: 1,
        minSymbols: 1
    }).withMessage('Strong Password is Required'),
], (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    // validate the form from client
    const errors = (0, express_validator_1.validationResult)(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({ errors: errors.array() });
    }
    try {
        let { email, password } = request.body;
        // check if email is exists
        const connection = yield DBUtil_1.DBUtil.connectToDB();
        const checkQuery = 'SELECT *  FROM users WHERE email = ?';
        const resultdata = yield queryDatabase(connection, checkQuery, [email]);
        console.log("resultdata: ", resultdata);
        if (resultdata && Array.isArray(resultdata) && resultdata.length > 0) {
            // The results object exists, is an array, and contains at least one record
            const userRecord = resultdata[0]; // Assuming it's the first record
            const userId = userRecord.user_id;
            console.log('User ID:', userId);
            let isMatch = yield bcryptjs_1.default.compare(password, userRecord.password);
            if (!isMatch) {
                return response.status(401).json({
                    msg: 'Invalid Credentials Password!'
                });
            }
            const payload = {
                user: {
                    id: userId,
                    email: userRecord.email
                }
            };
            const secretKey = process.env.JWT_SECRET_KEY;
            if (secretKey) {
                jsonwebtoken_1.default.sign(payload, secretKey, { expiresIn: 10000000 }, (err, encoded) => {
                    return response.status(200).json({
                        msg: 'Login user is Success',
                        token: encoded,
                        user: {
                            "user_id": userRecord.user_id,
                            "name": userRecord.name,
                            "email": userRecord.email,
                            "isAdmin": userRecord.isAdmin,
                            "avatar": userRecord.avatar
                        }
                    });
                });
            }
        }
        else {
            return response.status(401).json({
                msg: 'Invalid Credentials Email!'
            });
        }
    }
    catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
}));
/*
   @sno : 3
   @usage : Get User Info
   @url : http://127.0.0.1:9999/api/users/me
   @fields : no-fields
   @method : GET
   @access : PRIVATE
*/
userRouter.get('/me', TokenVerifier_1.default, (request, response) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let user = request.headers[constants_json_1.default.APP_CONSTANTS.USER];
        const connection = yield DBUtil_1.DBUtil.connectToDB();
        const userQuery = 'SELECT  user_id,name,email,isAdmin,avatar  FROM users WHERE user_id = ?';
        console.log('User ID:', user.id);
        const resultdata = yield queryDatabase(connection, userQuery, [user.id]);
        if (resultdata && Array.isArray(resultdata) && resultdata.length > 0) {
            // The results object exists, is an array, and contains at least one record
            const userRecord = resultdata[0]; // Assuming it's the first record
            const userId = userRecord.user_id;
            console.log('User ID:', userId);
            response.status(200).json({
                user: userRecord
            });
        }
        else {
            return response.status(404).json({
                msg: 'User not found!'
            });
        }
    }
    catch (error) {
        return response.status(500).json({
            errors: [
                {
                    msg: 'Server Error'
                }
            ]
        });
    }
}));
function queryDatabase(connection, sql, values) {
    return new Promise((resolve, reject) => {
        connection.query(sql, values, (error, results) => {
            if (error) {
                reject(error);
            }
            else {
                resolve(results);
            }
        });
    });
}
exports.default = userRouter;
