import mongoose, {Schema, Model} from 'mongoose';
import {IUser} from "../models/IUser";

const userSchema: Schema = new Schema<IUser>({
    name: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    isAdmin: {type: Boolean, required: true},
    avatar: {type: String, required: true}
}, {timestamps: true});

const UserTable: Model<IUser> = mongoose.model<IUser>('users', userSchema);
export default UserTable;