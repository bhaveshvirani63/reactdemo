"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const constants_json_1 = __importDefault(require("../constants.json"));
const tokenVerifier = (request, response, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // read the token from request
        let secretKey = process.env.JWT_SECRET_KEY;
        if (secretKey) {
            let token = request.headers[constants_json_1.default.APP_CONSTANTS.REQUEST_TOKEN_KEY];
            if (!token) {
                return response.status(401).json({
                    msg: 'No Token Provided!'
                });
            }
            if (typeof token === "string" && secretKey) {
                let decodeObj = yield jsonwebtoken_1.default.verify(token, secretKey);
                request.headers[constants_json_1.default.APP_CONSTANTS.USER] = decodeObj.user;
                next(); // passing to actual URL
            }
            else {
                return response.status(401).json({
                    msg: 'An Invalid Token!'
                });
            }
        }
    }
    catch (error) {
        return response.status(500).json({
            msg: 'Unauthorized!, its an invalid token'
        });
    }
});
exports.default = tokenVerifier;
