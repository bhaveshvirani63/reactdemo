"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DBUtil = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const mysql = require('mysql');
class DBUtil {
    static connectToDB() {
        return new Promise((resolve, reject) => {
            try {
                // const connection = mysql.createConnection({
                //     host: 'localhost', // Your MySQL server's host
                //     user: 'root',      // Your MySQL username
                //     password: "Temp@123", // Your MySQL password
                //     database: 'egaticom_react_social', // Your MySQL database name
                //     port: '3306'
                //   });
                const connection = mysql.createConnection({
                    host: 'localhost',
                    user: 'root',
                    password: "",
                    database: 'mysocialapp', // Your MySQL database name
                });
                connection.connect((error) => {
                    if (error) {
                        console.error('Error connecting to the database:', error);
                        reject(connection);
                        return;
                    }
                    console.log('Connected to the MySQL database');
                    resolve(connection);
                });
            }
            catch (error) {
                console.error(error);
            }
        });
    }
    static connectToDB1(dbUrl, dbName) {
        return new Promise((resolve, reject) => {
            try {
                mongoose_1.default.connect(dbUrl, {
                    dbName: dbName
                }).then((response) => {
                    resolve("Connected to DB is success");
                }).catch((error) => {
                    reject('Unable to Connect to DB!');
                    process.exit(0); // stop the node js server
                });
            }
            catch (error) {
                console.error(error);
            }
        });
    }
}
exports.DBUtil = DBUtil;
