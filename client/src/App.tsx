import React, {useEffect} from 'react';
import './App.css';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import NavBarSocial from "./modules/layout/components/navbar/NavBarSocial";
import Home from "./modules/layout/components/home/Home";
import About from "./modules/layout/components/about/About";
import DeveloperList from "./modules/developers/components/developer-list/DeveloperList";
import DeveloperDetails from "./modules/developers/components/developer-details/DeveloperDetails";
import UserRegister from "./modules/users/components/register/UserRegister";
import UserLogin from "./modules/users/components/login/UserLogin";
import Dashboard from "./modules/profiles/components/dashboard/Dashboard";
import AddEducation from "./modules/profiles/components/add-education/AddEducation";
import AddExperience from "./modules/profiles/components/add-experience/AddExperience";
import CreateProfile from "./modules/profiles/components/create-profile/CreateProfile";
import PostList from "./modules/posts/components/post-list/PostList";
import PostDetails from "./modules/posts/components/post-details/PostDetails";
import EditEducation from "./modules/profiles/components/edit-education/EditEducation";
import EditExperience from "./modules/profiles/components/edit-experience/EditExperience";
import EditProfile from "./modules/profiles/components/edit-profile/EditProfile";
import {ToastContainer} from "react-toastify";
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch} from "./redux/store";
import {RootUserState, usersFeatureKey} from "./redux/users/user.slice";
import * as userActions from './redux/users/user.actions';
import {AuthUtil} from "./util/AuthUtil";
import PrivateRoute from "./router/PrivateRoute";

interface IProps {
}

const App: React.FC<IProps> = ({}) => {

    const dispatch: AppDispatch = useDispatch();

    useEffect(() => {
        if (AuthUtil.isLoggedIn()) {
            dispatch(userActions.getUserInfoAction());
        }
    }, []);


    return (
        <>
            <BrowserRouter>
                <ToastContainer
                    position="top-center"
                    autoClose={2000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="light"
                />

                <Routes>
                    <Route path={'/'} element={<Home/>}/>
                    <Route path={'/about'} element={<About/>}/>
                    <Route path={'/developers'} element={<DeveloperList/>}/>
                    <Route path={'/developers/:developerId'} element={<DeveloperDetails/>}/>
                    <Route path={'/users/register'} element={<UserRegister/>}/>
                    <Route path={'/users/login'} element={<UserLogin/>}/>
                    <Route path={'/profiles/dashboard'} element={
                        <PrivateRoute>
                            <Dashboard/>
                        </PrivateRoute>
                    }/>
                    <Route path={'/profiles/education/add'} element={
                        <PrivateRoute>
                            <AddEducation/>
                        </PrivateRoute>
                    }/>
                    <Route path={'/profiles/education/edit/:educationId'} element={
                        <PrivateRoute>
                            <EditEducation/>
                        </PrivateRoute>
                    }/>
                    <Route path={'/profiles/experience/add'} element={
                        <PrivateRoute>
                            <AddExperience/>
                        </PrivateRoute>
                    }/>
                    <Route path={'/profiles/experience/edit/:experienceId'} element={
                        <PrivateRoute>
                            <EditExperience/>
                        </PrivateRoute>
                    }/>
                    <Route path={'/profiles/add'} element={
                        <PrivateRoute>
                            <CreateProfile/>
                        </PrivateRoute>
                    }/>
                    <Route path={'/profiles/edit/me'} element={
                        <PrivateRoute>
                            <EditProfile/>
                        </PrivateRoute>
                    }/>
                    <Route path={'/posts/all'} element={
                        <PrivateRoute>
                            <PostList/>
                        </PrivateRoute>
                    }/>
                    <Route path={'/posts/:postId'} element={
                        <PrivateRoute>
                            <PostDetails/>
                        </PrivateRoute>
                    }/>
                </Routes>
            </BrowserRouter>
        </>
    );
}

export default App;
