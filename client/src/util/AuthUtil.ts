import constants from '../constants.json';

export class AuthUtil {

    public static saveToken(token: string) {
        sessionStorage.setItem(constants.REACT_APP.TOKEN_KEY, token);
    }

    public static deleteToken() {
        sessionStorage.removeItem(constants.REACT_APP.TOKEN_KEY);
    }

    public static getToken(): string | null {
        return sessionStorage.getItem(constants.REACT_APP.TOKEN_KEY);
    }

    public static isLoggedIn(): boolean {
        let token: string | null = this.getToken();
        return !!token;
    }
}