import {createSlice, SerializedError} from "@reduxjs/toolkit";
import {DeveloperView} from "../../modules/developers/models/DeveloperView";
import * as developerActions from './developer.actions';

export const developersFeatureKey = "developersFeatureKey";

export interface RootDeveloperState {
    [developersFeatureKey]: InitialState
}

export interface InitialState {
    loading: boolean;
    errorMessage: SerializedError;
    developer: DeveloperView;
    developers: DeveloperView[];
}

const initialState: InitialState = {
    loading: false,
    errorMessage: {} as SerializedError,
    developer: {} as DeveloperView,
    developers: [] as DeveloperView[]
};

export const developerSlice = createSlice({
    name: 'developerSlice',
    initialState: initialState,
    reducers: {},
    extraReducers: (builder) => {

        // get all developers
        builder.addCase(developerActions.getAllDevelopersAction.pending, (state, action) => {
            state.loading = true;
        }).addCase(developerActions.getAllDevelopersAction.fulfilled, (state, action) => {
            state.loading = false;
            state.developers = action.payload.profiles;
        }).addCase(developerActions.getAllDevelopersAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
        })

        // get developer
        builder.addCase(developerActions.getDeveloperAction.pending, (state) => {
            state.loading = true;
        }).addCase(developerActions.getDeveloperAction.fulfilled, (state, action) => {
            state.loading = false;
            state.developer = action.payload.profile;
        }).addCase(developerActions.getDeveloperAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
        })
    }
})