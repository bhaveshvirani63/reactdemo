import {createAsyncThunk} from "@reduxjs/toolkit";
import {DeveloperService} from "../../modules/developers/services/DeveloperService";

// public
export const getAllDevelopersAction = createAsyncThunk('getAllDevelopersAction', async () => {
    let response = await DeveloperService.getAllDevelopers();
    return response.data;
});

// public
export const getDeveloperAction = createAsyncThunk('getDeveloperAction', async (profileId: string) => {
    let response = await DeveloperService.getDeveloper(profileId);
    return response.data;
});