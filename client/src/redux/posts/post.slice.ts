import * as postsActions from './post.actions';
import {createSlice, SerializedError} from "@reduxjs/toolkit";
import {PostView} from "../../modules/posts/models/PostView";
import * as developerActions from "../developers/developer.actions";

export const postsFeatureKey = "postsFeatureKey";

export interface RootPostState {
    [postsFeatureKey]: InitialState
}

export interface InitialState {
    loading: boolean;
    errorMessage: SerializedError;
    post: PostView;
    posts: PostView[];
    successMessage: string | null;
}

const initialState: InitialState = {
    loading: false,
    errorMessage: {} as SerializedError,
    post: {} as PostView,
    posts: [] as PostView[],
    successMessage: null
}

export const postSlice = createSlice({
    name: 'postSlice',
    initialState: initialState,
    reducers: {},
    extraReducers: (builder) => {
        // createPost
        builder.addCase(postsActions.createPostAction.pending, (state, action) => {
            state.loading = true;
        }).addCase(postsActions.createPostAction.fulfilled, (state, action) => {
            state.loading = false;
            state.posts = [action.payload.post, ...state.posts];
            state.successMessage = action.payload.msg;
        }).addCase(postsActions.createPostAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            // getAllPosts
            .addCase(postsActions.getAllPostsAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(postsActions.getAllPostsAction.fulfilled, (state, action) => {
            state.loading = false;
            state.posts = [...action.payload.posts.reverse()];
        }).addCase(postsActions.getAllPostsAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            // getPost
            .addCase(postsActions.getPostAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(postsActions.getPostAction.fulfilled, (state, action) => {
            state.loading = false;
            state.post = action.payload.post;
        }).addCase(postsActions.getPostAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            // deletePost
            .addCase(postsActions.deletePostAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(postsActions.deletePostAction.fulfilled, (state, action) => {
            state.loading = false;
            state.successMessage = action.payload.msg;
            state.posts = state.posts.filter((post) => {
                return post.post_id !== action.payload.post.post_id
            });
        }).addCase(postsActions.deletePostAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            // likePost
            .addCase(postsActions.likePostAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(postsActions.likePostAction.fulfilled, (state, action) => {
            state.loading = false;
            console.log("PPPP:", action.payload.post)
            console.log("PPPP:", state.posts)
            state.posts = state.posts.map(post => {
                console.log("PPPP-outsidesssss:", post.post_id)

                if (post.post_id == action.payload.post.post_id) {
                    console.log("PPPP-inside:", action.payload.post.post_id)
                    action.payload.post.post_id = Number(action.payload.post.post_id)
                    return action.payload.post;
                }
                console.log("PPPP-outside:", post.post_id)

                return post;
            });
            console.log("PPPP: after", state.posts)

        }).addCase(postsActions.likePostAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            // dislike Post
            .addCase(postsActions.unLikePostAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(postsActions.unLikePostAction.fulfilled, (state, action) => {
            state.loading = false;
            state.posts = state.posts.map(post => {
                if (post.post_id == action.payload.post.post_id) {
                    action.payload.post.post_id = Number(action.payload.post.post_id)

                    return action.payload.post;
                }
                return post;
            });
        }).addCase(postsActions.unLikePostAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            // createComment
            .addCase(postsActions.createCommentAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(postsActions.createCommentAction.fulfilled, (state, action) => {
            state.loading = false;
            state.posts = state.posts.map(post => {
                if (post.post_id == action.payload.post.post_id) {
                    action.payload.post.post_id = Number(action.payload.post.post_id)

                    return action.payload.post;
                }
                return post;
            });
            state.successMessage = action.payload.msg;
        }).addCase(postsActions.createCommentAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            // deleteComment
            .addCase(postsActions.deleteCommentAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(postsActions.deleteCommentAction.fulfilled, (state, action) => {
            state.loading = false;
            state.posts = state.posts.map(post => {
                if (post.post_id === action.payload.post.post_id) {
                    return action.payload.post;
                }
                return post;
            });
            state.successMessage = action.payload.msg;
        }).addCase(postsActions.deleteCommentAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })
    }
});