import {createAsyncThunk} from "@reduxjs/toolkit";
import {PostService} from "../../modules/posts/services/PostService";
import {IComment, PostView} from "../../modules/posts/models/PostView";

// private : createPost
export const createPostAction = createAsyncThunk('createPostAction', async (payload: {post: PostView, imageFile:any}) => {
    let {post, imageFile} = payload;
    console.log('POOOOOO', post)

    let response = await PostService.createPost(post,imageFile);
    return response.data;
});

// private : getAllPosts
export const getAllPostsAction = createAsyncThunk('getAllPostsAction', async () => {
    let response = await PostService.getAllPosts();
    return response.data;
});

// private : getPost
export const getPostAction = createAsyncThunk('getPostAction', async (postId: string) => {
    let response = await PostService.getPost(postId);
    return response.data;
});

// private : deletePost
export const deletePostAction = createAsyncThunk('deletePostAction', async (postId: string) => {
    let response = await PostService.deletePost(postId);
    return response.data;
});

// private : likePost
export const likePostAction = createAsyncThunk('likePostAction', async (postId: string) => {
    let response = await PostService.likePost(postId);
    console.log('resposne: ', response)
    return response.data;
});


// private : unLikePost
export const unLikePostAction = createAsyncThunk('unLikePostAction', async (postId: string) => {
    let response = await PostService.unLikePost(postId);
    return response.data;
});

// private : createComment
export const createCommentAction = createAsyncThunk('createCommentAction', async (payload: { postId: string, comment: IComment }) => {
    let {comment, postId} = payload;
    let response = await PostService.createComment(postId, comment);
    return response.data;
});

// private : deleteComment
export const deleteCommentAction = createAsyncThunk('deleteCommentAction', async (payload: { commentId: string, postId: string }) => {
    let {commentId, postId} = payload;
    let response = await PostService.deleteComment(commentId, postId);
    return response.data;
});