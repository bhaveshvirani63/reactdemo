import {combineReducers} from "@reduxjs/toolkit";
import * as userReducer from './users/user.slice';
import * as developerReducer from './developers/developer.slice';
import * as profileReducer from './profiles/profile.slice';
import * as postReducer from './posts/post.slice';

const rootReducer = combineReducers({
    [userReducer.usersFeatureKey]: userReducer.userSlice.reducer,
    [developerReducer.developersFeatureKey]: developerReducer.developerSlice.reducer,
    [profileReducer.profileFeatureKey]: profileReducer.profileSlice.reducer,
    [postReducer.postsFeatureKey]: postReducer.postSlice.reducer
});

export default rootReducer;