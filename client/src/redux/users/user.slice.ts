import * as userActions from './user.actions';
import {createSlice, SerializedError} from "@reduxjs/toolkit";
import {UserView} from "../../modules/users/models/UserView";
import {AuthUtil} from "../../util/AuthUtil";
import {TokenUtil} from "../../util/TokenUtil";
import {ToastUtil} from "../../util/ToastUtil";

export const usersFeatureKey = "usersFeatureKey";

export interface RootUserState {
    [usersFeatureKey]: InitialState
}

export interface InitialState {
    loading: boolean;
    errorMessage: SerializedError;
    user: UserView;
    token: string | null;
    isAuthenticated: boolean;
    successMessage: string;
}

const initialState: InitialState = {
    loading: false,
    errorMessage: {} as SerializedError,
    user: {} as UserView,
    token: null,
    isAuthenticated: false,
    successMessage: ""
};

export const userSlice = createSlice({
    name: 'userSlice',
    initialState: initialState,
    reducers: {
        logOutAction: (state, action) => {
            ToastUtil.displaySuccessToast('LogOut is Success!');
            AuthUtil.deleteToken();
            state.user = {} as UserView;
            state.token = null;
            state.isAuthenticated = false;
        }
    },
    extraReducers: (builder) => {
        // register a User
        builder.addCase(userActions.registerUserAction.pending, (state) => {
            state.loading = true;
        }).addCase(userActions.registerUserAction.fulfilled, (state, action) => {
            state.loading = false;
            state.successMessage = action.payload.msg;
        }).addCase(userActions.registerUserAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = "";
        })

            // login a User
            .addCase(userActions.loginUserAction.pending, (state) => {
                state.loading = true;
            }).addCase(userActions.loginUserAction.fulfilled, (state, action) => {
            state.loading = false;
            state.successMessage = action.payload.msg;
            state.token = action.payload.token;
            AuthUtil.saveToken(action.payload.token); // save token to session storage
            state.isAuthenticated = true;
            state.user = action.payload.user
        }).addCase(userActions.loginUserAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = "";
            state.token = null;
            AuthUtil.deleteToken(); // delete the token
            state.isAuthenticated = false;
        })

            // get User Info
            .addCase(userActions.getUserInfoAction.pending, (state) => {
                state.loading = true;
            }).addCase(userActions.getUserInfoAction.fulfilled, (state, action) => {
            state.loading = false;
            state.successMessage = "";
            state.user = action.payload.user;
            state.isAuthenticated = true;
        }).addCase(userActions.getUserInfoAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = "";
            state.user = {} as UserView;
            state.token = null;
            state.isAuthenticated = false;
            AuthUtil.deleteToken(); // delete the token
        })
    }
})
export const {logOutAction} = userSlice.actions;
