import {createAsyncThunk} from "@reduxjs/toolkit";
import {UserView} from "../../modules/users/models/UserView";
import {UserService} from "../../modules/users/services/UserService";
import {TokenUtil} from "../../util/TokenUtil";

export const registerUserAction = createAsyncThunk('registerUserAction', async (user: UserView) => {
    let response = await UserService.registerUser(user);
    return response.data;
});

export const loginUserAction = createAsyncThunk('loginUserAction', async (user: UserView) => {
    let response = await UserService.loginUser(user);
    return response.data;
});

// private url
export const getUserInfoAction = createAsyncThunk('getUserInfoAction', async () => {
    if (TokenUtil.isSetToken()) {
        let response = await UserService.getUserInfo();
        return response.data;
    }
});

