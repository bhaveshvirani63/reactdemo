import {createAsyncThunk} from "@reduxjs/toolkit";
import {ProfileService} from "../../modules/profiles/services/ProfileService";
import {IEducation, IExperience, ProfileView} from "../../modules/profiles/models/ProfileView";
import {TokenUtil} from "../../util/TokenUtil";

// private : createProfile
export const createProfileAction = createAsyncThunk('createProfileAction', async (profile: any) => {
    if (TokenUtil.isSetToken()) {
        let response = await ProfileService.createProfile(profile);
        return response.data;
    }
});

// private : getMyProfile
export const getMyProfileAction = createAsyncThunk('getMyProfileAction', async () => {
    if (TokenUtil.isSetToken()) {
        let response = await ProfileService.getMyProfile();
        return response.data;
    }
});

// private : Update a Profile
export const updateProfileAction = createAsyncThunk('updateProfileAction', async (payload: { profileId: string, profile: any }) => {
    let {profileId, profile} = payload;
    if (TokenUtil.isSetToken()) {
        let response = await ProfileService.updateProfile(profileId, profile);
        return response.data;
    }
});

// private : getUserProfile
export const getUserProfileAction = createAsyncThunk('getUserProfileAction', async (userId: string) => {
    if (TokenUtil.isSetToken()) {
        let response = await ProfileService.getUserProfile(userId);
        return response.data;
    }
});

// private : deleteAccount
export const deleteAccountAction = createAsyncThunk('deleteAccountAction', async (userId: string) => {
    if (TokenUtil.isSetToken()) {
        let response = await ProfileService.deleteAccount(userId);
        return response.data;
    }
});

// private : addExperienceToProfile
export const addExperienceToProfileAction = createAsyncThunk('addExperienceToProfileAction', async (experience: IExperience) => {
    if (TokenUtil.isSetToken()) {
        let response = await ProfileService.addExperienceToProfile(experience);
        return response.data;
    }
});


// private : deleteExperienceOfProfile
export const deleteExperienceOfProfileAction = createAsyncThunk('deleteExperienceOfProfileAction', async (experienceId: string, {dispatch}) => {
    if (TokenUtil.isSetToken()) {
        let response = await ProfileService.deleteExperienceOfProfile(experienceId);
        return response.data;
    }
});

// private : addEducationToProfile
export const addEducationToProfileAction = createAsyncThunk('addEducationToProfileAction', async (education: IEducation) => {
    if (TokenUtil.isSetToken()) {
        let response = await ProfileService.addEducationToProfile(education);
        return response.data;
    }
});


// private : deleteEducationOfProfile
export const deleteEducationOfProfileAction = createAsyncThunk('deleteEducationOfProfileAction', async (educationId: string) => {
    if (TokenUtil.isSetToken()) {
        let response = await ProfileService.deleteEducationOfProfile(educationId);
        return response.data;
    }
});