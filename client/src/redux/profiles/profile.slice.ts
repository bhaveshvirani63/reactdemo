import {createSlice, SerializedError} from "@reduxjs/toolkit";
import {ProfileView} from "../../modules/profiles/models/ProfileView";
import * as profileActions from './profile.actions';

export const profileFeatureKey = "profileFeatureKey";

export interface RootProfileState {
    [profileFeatureKey]: InitialState
}

export interface InitialState {
    loading: boolean;
    errorMessage: SerializedError;
    profile: ProfileView;
    profiles: ProfileView[];
    successMessage: string | null;
}

const initialState: InitialState = {
    loading: false,
    errorMessage: {} as SerializedError,
    profile: {} as ProfileView,
    profiles: [] as ProfileView[],
    successMessage: null
};

export const profileSlice = createSlice({
    name: 'profileSlice',
    initialState: initialState,
    reducers: {},
    extraReducers: (builder) => {
        // createProfileAction
        builder.addCase(profileActions.createProfileAction.pending, (state, action) => {
            state.loading = true;
        }).addCase(profileActions.createProfileAction.fulfilled, (state, action) => {
            state.loading = false;
            state.successMessage = action.payload.msg;
        }).addCase(profileActions.createProfileAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            // get my profile
            .addCase(profileActions.getMyProfileAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(profileActions.getMyProfileAction.fulfilled, (state, action) => {
            state.loading = false;
            if (action.payload.profile) {
                state.profile = action.payload.profile;
            } else {
                state.profile = {} as ProfileView
            }

        }).addCase(profileActions.getMyProfileAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            // update Profile
            .addCase(profileActions.updateProfileAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(profileActions.updateProfileAction.fulfilled, (state, action) => {
            state.loading = false;
            state.profile = action.payload.profile;
            state.successMessage = action.payload.msg;
        }).addCase(profileActions.updateProfileAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            // getUserProfile
            .addCase(profileActions.getUserProfileAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(profileActions.getUserProfileAction.fulfilled, (state, action) => {
            state.loading = false;
            state.profile = action.payload.profile;
        }).addCase(profileActions.getUserProfileAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            // Delete Account
            .addCase(profileActions.deleteAccountAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(profileActions.deleteAccountAction.fulfilled, (state, action) => {
            state.loading = false;
            state.successMessage = action.payload.msg;
            state.profile = {} as ProfileView;
        }).addCase(profileActions.deleteAccountAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            //  Add an experience to a Profile
            .addCase(profileActions.addExperienceToProfileAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(profileActions.addExperienceToProfileAction.fulfilled, (state, action) => {
            state.loading = false;
            state.successMessage = action.payload.msg;
        }).addCase(profileActions.addExperienceToProfileAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            //  Delete an experience of a Profile
            .addCase(profileActions.deleteExperienceOfProfileAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(profileActions.deleteExperienceOfProfileAction.fulfilled, (state, action) => {
            state.loading = false;
            state.successMessage = action.payload.msg;
        }).addCase(profileActions.deleteExperienceOfProfileAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            //  Add an education to a Profile
            .addCase(profileActions.addEducationToProfileAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(profileActions.addEducationToProfileAction.fulfilled, (state, action) => {
            state.loading = false;
            state.successMessage = action.payload.msg;
        }).addCase(profileActions.addEducationToProfileAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })

            //  Delete an education of a Profile
            .addCase(profileActions.deleteEducationOfProfileAction.pending, (state, action) => {
                state.loading = true;
            }).addCase(profileActions.deleteEducationOfProfileAction.fulfilled, (state, action) => {
            state.loading = false;
            state.successMessage = action.payload.msg;
        }).addCase(profileActions.deleteEducationOfProfileAction.rejected, (state, action) => {
            state.loading = false;
            state.errorMessage = action.error;
            state.successMessage = null;
        })
    }
});