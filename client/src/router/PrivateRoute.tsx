import React from 'react';
import {Navigate} from 'react-router-dom';
import {AuthUtil} from "../util/AuthUtil";
import {useSelector} from "react-redux";
import {RootUserState, usersFeatureKey} from "../redux/users/user.slice";

function PrivateRoute({children}: any) {

    const userState = useSelector((state: RootUserState) => {
        return state[usersFeatureKey];
    })

    const {isAuthenticated} = userState;

    const auth = AuthUtil.isLoggedIn();
    return auth ? children : <>
        <Navigate to="/"/>
    </>;
}

export default PrivateRoute;