export interface ProfileView {
    user?: string;
    profile_id?: string;
    name?: string;
    avatar?: string;
    company: string;
    website: string;
    location: string;
    designation: string;
    skills: string;
    bio: string;
    githubUsername: string;
    experience?: IExperience[],
    education?: IEducation[],
    social: ISocial,
    createdAt?: string;
    updatedAt?: string;
}

export interface IExperience {
    experience_id?: string;
    title: string;
    company: string;
    location: string;
    from: string;
    to?: string;
    current?: boolean;
    description: string;
}

export interface IEducation {
    education_id?: string;
    school: string;
    degree: string;
    fieldOfStudy: string;
    from: string;
    to?: string;
    current?: boolean;
    description: string;
}

export interface ISocial {
    youtube: string;
    twitter: string;
    facebook: string;
    linkedin: string;
    instagram: string;
}

