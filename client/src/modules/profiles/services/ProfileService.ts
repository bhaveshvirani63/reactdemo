import axios from "axios";
import {IEducation, IExperience, ProfileView} from "../models/ProfileView";

export class ProfileService {

    private static serverUrl = process.env.REACT_APP_EXPRESS_SERVER;
    private static profilesAPI: string = `${this.serverUrl}/api/profiles`

    // create profile
    public static createProfile(profile: any) {
        return axios.post(`${this.profilesAPI}/`, profile);
    }

    // Get my Profile
    public static getMyProfile() {
        return axios.get(`${this.profilesAPI}/me`);
    }

    // Update a Profile
    public static updateProfile(profileId: string, profile: any) {
        return axios.put(`${this.profilesAPI}/${profileId}`, profile);
    }

    // Get Profile of a User
    public static getUserProfile(userId: string) {
        return axios.get(`${this.profilesAPI}/users/${userId}`);
    }

    // Delete a Profile , User , Post
    public static deleteAccount(userId: string) {
        return axios.delete(`${this.profilesAPI}/users/${userId}`);
    }

    // Add an experience to a Profile
    public static addExperienceToProfile(experience: IExperience) {
        return axios.post(`${this.profilesAPI}/experience/`, experience);
    }

    // Delete an experience of a Profile
    public static deleteExperienceOfProfile(experienceId: string) {
        return axios.delete(`${this.profilesAPI}/experience/${experienceId}`);
    }

    // Add an education of a Profile
    public static addEducationToProfile(education: IEducation) {
        return axios.post(`${this.profilesAPI}/education/`, education);
    }

    // Delete an education of a Profile
    public static deleteEducationOfProfile(educationId: string) {
        return axios.delete(`${this.profilesAPI}/education/${educationId}`);
    }
}