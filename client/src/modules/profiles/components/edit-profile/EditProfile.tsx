import React, {useEffect, useState} from 'react';
import {Button, Col, Container, Form, Row} from "react-bootstrap";
import {Link, useNavigate} from "react-router-dom";
import NavBarSocial from "../../../layout/components/navbar/NavBarSocial";
import * as profileActions from '../../../../redux/profiles/profile.actions';
import {useSelector, useDispatch} from "react-redux";
import {AppDispatch} from "../../../../redux/store";
import {profileFeatureKey, RootProfileState} from "../../../../redux/profiles/profile.slice";
import {ToastUtil} from "../../../../util/ToastUtil";

interface IProps {
}

let EditProfile: React.FC<IProps> = ({}) => {
    const [validated, setValidated] = useState(false);

    const dispatch: AppDispatch = useDispatch();
    const navigate = useNavigate();

    // get profile Data from redux store
    const profileState = useSelector((state: RootProfileState) => {
        return state[profileFeatureKey];
    });

    const {loading, profile: reduxProfile} = profileState;

    const [designations] = useState<string[]>([
        "Software Engineer",
        "Sr. Software Engineer",
        "Tech Lead",
        "QA Lead",
        "Project Manager",
        "Director",
        "Other"
    ]);

    const [profile, setProfile] = useState<any>({
        company: "",
        website: "",
        location: "",
        designation: "",
        skills: "",
        bio: "",
        githubUsername: "",
        // facebook: "",
        // twitter: "",
        // youtube: "",
        // linkedin: "",
        // instagram: ""
    });

    const updateInput = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
        setProfile((prevState: any) => {
            return {
                ...prevState,
                [event.target.name]: event.target.value
            }
        })
    };


    useEffect(() => {
        dispatch(profileActions.getMyProfileAction());
    }, []);


    useEffect(() => {
        console.log('reduxProfile: ',reduxProfile)
        if (Object.keys(reduxProfile).length > 0) {
            setProfile((preState: any) => {
                return {
                    ...preState,
                    company: reduxProfile.company ? reduxProfile.company : "",
                    website: reduxProfile.website ? reduxProfile.website : "",
                    location: reduxProfile.location ? reduxProfile.location : "",
                    designation: reduxProfile.designation ? reduxProfile.designation : "",
                    skills: reduxProfile.skills ? reduxProfile.skills.toString() : "",
                    bio: reduxProfile.bio ? reduxProfile.bio : "",
                    githubUsername: reduxProfile.githubUsername ? reduxProfile.githubUsername : ""
                    // facebook: reduxProfile.social.facebook ? reduxProfile.social.facebook : "",
                    // twitter: reduxProfile.social.twitter ? reduxProfile.social.twitter : "",
                    // linkedin: reduxProfile.social.linkedin ? reduxProfile.social.linkedin : "",
                    // youtube: reduxProfile.social.youtube ? reduxProfile.social.youtube : "",
                    // instagram: reduxProfile.social.instagram ? reduxProfile.social.instagram : ""
                }
            })
        }
    }, [reduxProfile]);

    const handleSubmit = (event: any) => {
        console.log('Profile ID: ',reduxProfile.profile_id)
        event.preventDefault();
        const form = event.currentTarget;
        if (form.checkValidity() === true && reduxProfile.profile_id) {
            dispatch(profileActions.updateProfileAction({
                profile: profile,
                profileId: reduxProfile.profile_id
            })).then((response: any) => {
                if (response.error) {
                    ToastUtil.displayErrorToast(response.error.message);
                } else {
                    ToastUtil.displaySuccessToast('Profile is Updated!');
                    navigate('/profiles/dashboard');
                }
            })
        }
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        setValidated(true);
    };

    return (
        <>
            <NavBarSocial/>
            <Container className="mt-3">
                <Row>
                    <Col>
                        <h3 className="text-success">
                            <i className="fa fa-user-circle"></i> Edit your Profile
                        </h3>
                        <p className="fst-italic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium
                            ad adipisci, assumenda atque deleniti deserunt dolorem doloribus maxime neque, odit placeat
                            quas quibusdam quidem quis quisquam quos reprehenderit similique ut!</p>
                    </Col>
                </Row>

                <Row>
                    <Col xs={5}>
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                            <p>Profile Details</p>
                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'company'}
                                    value={profile.company}
                                    onChange={updateInput}
                                    type="text" placeholder="Company" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Company.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'website'}
                                    value={profile.website}
                                    onChange={updateInput}
                                    type="website" placeholder="Website" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Website.
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'location'}
                                    value={profile.location}
                                    onChange={updateInput}
                                    type="text" placeholder="Location" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Location.
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-2">
                                <Form.Select
                                    name={'designation'}
                                    value={profile.designation}
                                    onChange={updateInput} required>
                                    <option
                                        value=""
                                    >Select Designation
                                    </option>
                                    {
                                        designations.map(designation => {
                                            return (
                                                <option key={designation}
                                                        value={designation}
                                                >{designation}</option>
                                            )
                                        })
                                    }
                                </Form.Select>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Designation.
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'skills'}
                                    value={profile.skills}
                                    onChange={updateInput}
                                    type="text" placeholder="Skills" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Skills.
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'bio'}
                                    value={profile.bio}
                                    onChange={updateInput}
                                    as="textarea" rows={3} placeholder="Bio" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Bio.
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'githubUsername'}
                                    value={profile.githubUsername}
                                    onChange={updateInput}
                                    type="text" placeholder="Github Username" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid GithubUsername.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <hr/>
                            {/* <p>Social Details</p> */}
                            {/* <Form.Group className="mb-2">
                                <Form.Control
                                    name={'facebook'}
                                    value={profile.facebook}
                                    onChange={updateInput}
                                    type="text" placeholder="facebook" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid facebook.
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'twitter'}
                                    value={profile.twitter}
                                    onChange={updateInput}
                                    type="text" placeholder="twitter" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Twitter.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'youtube'}
                                    value={profile.youtube}
                                    onChange={updateInput}
                                    type="text" placeholder="youtube" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Youtube.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'linkedin'}
                                    value={profile.linkedin}
                                    onChange={updateInput}
                                    type="text" placeholder="linkedin" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Linkedin.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'instagram'}
                                    value={profile.instagram}
                                    onChange={updateInput}
                                    type="text" placeholder="Instagram" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Instagram.
                                </Form.Control.Feedback>
                            </Form.Group> */}
                            <Button variant="success" type="submit" className="me-1">
                                Update
                            </Button>
                            <Link to={'/profiles/dashboard'}>
                                <Button variant="dark" type="button">
                                    Cancel
                                </Button>
                            </Link>
                        </Form>
                    </Col>
                </Row>
            </Container>

            <div style={{marginBottom: '100px'}}></div>
        </>
    )
};
export default EditProfile;