import React, {useEffect} from 'react';
import {Button, Col, Container, Row, Table} from "react-bootstrap";
import {Link} from "react-router-dom";
import NavBarSocial from "../../../layout/components/navbar/NavBarSocial";
import * as profileActions from '../../../../redux/profiles/profile.actions';
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch} from "../../../../redux/store";
import {profileFeatureKey, RootProfileState} from "../../../../redux/profiles/profile.slice";
import Spinner from "../../../layout/components/spinner/Spinner";
import {RootUserState, usersFeatureKey} from "../../../../redux/users/user.slice";
import {ToastUtil} from "../../../../util/ToastUtil";

interface IProps {
}

let Dashboard: React.FC<IProps> = ({}) => {
    const dispatch: AppDispatch = useDispatch();

    // get profile state from redux
    const profileState = useSelector((state: RootProfileState) => {
        return state[profileFeatureKey];
    });

    // get user info from redux store
    const userState = useSelector((state: RootUserState) => {
        return state[usersFeatureKey];
    });

    let {loading, profile, successMessage} = profileState;
    let {user} = userState;


    useEffect(() => {
        dispatch(profileActions.getMyProfileAction());
    }, []);

    useEffect(() => {
        dispatch(profileActions.getMyProfileAction());
    }, [successMessage]);

    const clickDeleteExperience = (expId: string | undefined) => {
        console.log("Experience Id is : ",expId)

        if (expId) {
            dispatch(profileActions.deleteExperienceOfProfileAction(expId)).then((response: any) => {
                if (response.error) {
                    ToastUtil.displayErrorToast(response.error.message);
                } else {
                    ToastUtil.displayInfoToast('Experience is Deleted!');
                }
            })
        }
    };

    const clickDeleteEducation = (eduId: string | undefined) => {
        console.log("education Id is : ",eduId)
        if (eduId) {
            dispatch(profileActions.deleteEducationOfProfileAction(eduId)).then((response: any) => {
                if (response.error) {
                    ToastUtil.displayErrorToast(response.error.message);
                } else {
                    ToastUtil.displayInfoToast('Education is Deleted!');
                }
            })
        }
    };

    return (
        <>
            <NavBarSocial/>
            {
                loading && <Spinner/>
            }
            <Container className={"mt-3"}>
                <Row>
                    <Col>
                        <h3 className="text-success">
                            <i className="fa fa-dashboard text-success"></i> Dashboard</h3>
                        {
                            Object.keys(user).length > 0 &&
                            <p>Welcome Mr. <span className="fw-bold">{user.name}</span></p>
                        }
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {
                            Object.keys(profile).length > 0 ?
                                <>
                                    <Link to={'/profiles/edit/me'}>
                                        <Button variant={'warning'} className="m-1">
                                            <i className="fa fa-user-secret"></i> Edit Profile</Button>
                                    </Link>
                                    <Link to={'/profiles/experience/add'}>
                                        <Button variant={'warning'} className="m-1">
                                            <i className="fa fa-black-tie"></i> Add Experience</Button>
                                    </Link>
                                    <Link to={'/profiles/education/add'}>
                                        <Button variant={'warning'} className="m-1">
                                            <i className="fa fa-graduation-cap"></i> Add Education</Button>
                                    </Link>
                                </> : <>
                                    <Link to={'/profiles/add'}>
                                        <Button variant={'success'} className="m-1">
                                            <i className="fa fa-user-plus"></i> Create Profile</Button>
                                    </Link>
                                </>
                        }
                        <hr/>
                    </Col>
                </Row>

                {
                    Object.keys(profile).length > 0 &&

                    <>
                        {
                            profile.experience && profile.experience?.length > 0 &&
                            <Row>
                                <Col>
                                    <h4 className="text-success">Experience Details</h4>
                                    <small className="fst-italic">Lorem ipsum dolor sit amet, consectetur adipisicing
                                        elit.
                                        Architecto autem nisi non perspiciatis vitae! Doloremque ea earum harum magni
                                        maiores minima
                                        non quia recusandae repellendus! Id itaque quam veritatis. Nostrum?</small>
                                    <Table bordered striped className="text-center shadow-lg">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Company</th>
                                            <th>Location</th>
                                            <th>From Date</th>
                                            <th>To Date</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            profile.experience.map((exp) => {
                                                return (
                                                    <tr key={exp.experience_id}>
                                                        <td>{exp.title}</td>
                                                        <td>{exp.company}</td>
                                                        <td>{exp.location}</td>
                                                        <td>{exp.from}</td>
                                                        <td>{exp.to}</td>
                                                        <td>
                                                            <Link  
                                                                 to={`/profiles/experience/edit/${exp.experience_id}`} >
                                                                <Button disabled={true} variant="primary"
                                                                        className="me-2">
                                                                    <i className="fa fa-edit"></i>
                                                                </Button>
                                                            </Link>
                                                            <Button variant="danger"
                                                                    onClick={() => clickDeleteExperience(exp.experience_id)}>
                                                                <i className="fa fa-trash"></i>
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </Table>
                                </Col>
                                <hr/>
                            </Row>
                        }

                        {
                            profile.education && profile.education?.length > 0 &&
                            <Row>
                                <Col>
                                    <h4 className="text-success">Education Details</h4>
                                    <small className="fst-italic">Lorem ipsum dolor sit amet, consectetur adipisicing
                                        elit.
                                        Architecto autem nisi non perspiciatis vitae! Doloremque ea earum harum magni
                                        maiores minima
                                        non quia recusandae repellendus! Id itaque quam veritatis. Nostrum?</small>
                                    <Table bordered striped className="text-center shadow-lg">
                                        <thead>
                                        <tr>
                                            <th>School</th>
                                            <th>Degree</th>
                                            <th>FieldOfStudy</th>
                                            <th>From Date</th>
                                            <th>To Date</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            profile.education.map((edu) => {
                                                return (
                                                    <tr key={edu.education_id}>
                                                        <td>{edu.school}</td>
                                                        <td>{edu.degree}</td>
                                                        <td>{edu.fieldOfStudy}</td>
                                                        <td>{edu.from}</td>
                                                        <td>{edu.to}</td>
                                                        <td>
                                                            <Link to={'/'}
                                                                /*to={`/profiles/experience/edit/${exp._id}`}*/>
                                                                <Button disabled={true} variant="primary"
                                                                        className="me-2">
                                                                    <i className="fa fa-edit"></i>
                                                                </Button>
                                                            </Link>
                                                            <Button variant="danger"
                                                                    onClick={() => clickDeleteEducation(edu.education_id)}>
                                                                <i className="fa fa-trash"></i>
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </Table>
                                </Col>
                            </Row>
                        }
                    </>
                }

            </Container>
        </>
    )
};
export default Dashboard;