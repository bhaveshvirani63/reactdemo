import React, {useState} from 'react';
import {Button, Col, Container, Form, Row} from "react-bootstrap";
import {Link, useNavigate} from "react-router-dom";
import NavBarSocial from "../../../layout/components/navbar/NavBarSocial";
import {useDispatch} from "react-redux";
import * as profileActions from '../../../../redux/profiles/profile.actions';
import {AppDispatch} from "../../../../redux/store";
import {ToastUtil} from "../../../../util/ToastUtil";

interface IProps {
}

let CreateProfile: React.FC<IProps> = ({}) => {
    const [validated, setValidated] = useState(false);
    const dispatch: AppDispatch = useDispatch();
    const navigate = useNavigate();

    const [designations] = useState<string[]>([
        "Software Engineer",
        "Sr. Software Engineer",
        "Tech Lead",
        "QA Lead",
        "Project Manager",
        "Director",
        "Other"
    ]);

    const [profile, setProfile] = useState<any>({
        company: "",
        website: "",
        location: "",
        designation: "",
        skills: "",
        bio: "",
        githubUsername: "",
        facebook: "",
        twitter: "",
        youtube: "",
        linkedin: "",
        instagram: ""
    });

    const updateInput = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
        setProfile((prevState: any) => {
            return {
                ...prevState,
                [event.target.name]: event.target.value
            }
        })
    };

    const handleSubmit = (event: any) => {
        event.preventDefault();
        const form = event.currentTarget;
        if (form.checkValidity() === true) {
            dispatch(profileActions.createProfileAction(profile)).then((response: any) => {
                if (response.error) {
                    ToastUtil.displayErrorToast(response.error.message);
                } else {
                    ToastUtil.displaySuccessToast('Profile is Created!');
                    navigate('/profiles/dashboard');
                }
            })
        }
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        setValidated(true);
    };

    return (
        <>
            <NavBarSocial/>
            <Container className="mt-3">
                <Row>
                    <Col>
                        <h3 className="text-success">
                            <i className="fa fa-user-circle"></i> Create a Profile
                        </h3>
                        <p className="fst-italic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus
                            quisquam, suscipit! Ab aut dicta doloribus enim iure laboriosam, maxime nam nihil, odio
                            officia reiciendis repudiandae sapiente similique sunt tempora voluptates.</p>
                    </Col>
                </Row>

                <Row>
                    <Col xs={5}>
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                            <p>Profile Details</p>
                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'company'}
                                    value={profile.company}
                                    onChange={updateInput}
                                    type="text" placeholder="Company" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Company.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'website'}
                                    value={profile.website}
                                    onChange={updateInput}
                                    type="website" placeholder="Website" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Website.
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'location'}
                                    value={profile.location}
                                    onChange={updateInput}
                                    type="text" placeholder="Location" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Location.
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-2">
                                <Form.Select
                                    name={'designation'}
                                    value={profile.designation}
                                    onChange={updateInput} required>
                                    <option
                                        value=""
                                    >Select Designation
                                    </option>
                                    {
                                        designations.map(designation => {
                                            return (
                                                <option key={designation}
                                                        value={designation}
                                                >{designation}</option>
                                            )
                                        })
                                    }
                                </Form.Select>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Designation.
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'skills'}
                                    value={profile.skills}
                                    onChange={updateInput}
                                    type="text" placeholder="Skills" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Skills.
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'bio'}
                                    value={profile.bio}
                                    onChange={updateInput}
                                    as="textarea" rows={3} placeholder="Bio" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Bio.
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-2">
                                <Form.Control
                                    name={'githubUsername'}
                                    value={profile.githubUsername}
                                    onChange={updateInput}
                                    type="text" placeholder="Github Username" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid GithubUsername.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <hr/>
                            
                            <Button variant="success" type="submit" className="me-1">
                                Create
                            </Button>
                            <Link to={'/profiles/dashboard'}>
                                <Button variant="dark" type="button">
                                    Cancel
                                </Button>
                            </Link>
                        </Form>
                    </Col>
                </Row>
            </Container>

            <div style={{marginBottom: '100px'}}></div>
        </>
    )
};
export default CreateProfile;