import React, {useState} from 'react';
import {Button, Col, Container, Form, InputGroup, Row} from "react-bootstrap";
import {Link} from "react-router-dom";
import NavBarSocial from "../../../layout/components/navbar/NavBarSocial";

interface IProps {
}

let EditEducation: React.FC<IProps> = ({}) => {
    let [currentExp, setCurrentExp] = useState<boolean>(false);

    return (
        <>
            <NavBarSocial/>
            <Container className="mt-3">
                <Row>
                    <Col>
                        <h3 className="text-success">
                            <i className="fa fa-graduation-cap"></i> Edit Education
                        </h3>
                        <p className="fst-italic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium
                            ad adipisci, assumenda atque deleniti deserunt dolorem doloribus maxime neque, odit placeat
                            quas quibusdam quidem quis quisquam quos reprehenderit similique ut!</p>
                    </Col>
                </Row>

                <Row>
                    <Col xs={4}>
                        <InputGroup className="mb-2">
                            <InputGroup.Text id="basic-addon1"
                                             className="bg-light-green text-dark">School</InputGroup.Text>
                            <Form.Control
                                type="text"
                                placeholder="School"
                            />
                        </InputGroup>
                        <InputGroup className="mb-2">
                            <InputGroup.Text id="basic-addon1"
                                             className="bg-light-green text-dark">Degree</InputGroup.Text>
                            <Form.Control
                                type="text"
                                placeholder="Degree"
                            />
                        </InputGroup>
                        <InputGroup className="mb-2">
                            <InputGroup.Text id="basic-addon1"
                                             className="bg-light-green text-dark">Field Of Study</InputGroup.Text>
                            <Form.Control
                                type="text"
                                placeholder="Field Of Study"
                            />
                        </InputGroup>
                        <InputGroup className="mb-2">
                            <InputGroup.Text id="basic-addon1" className="bg-light-green text-dark">From
                                Date</InputGroup.Text>
                            <Form.Control
                                type="date"
                                placeholder="From Date"
                            />
                        </InputGroup>
                        <InputGroup className="mb-2">
                            <InputGroup.Text id="basic-addon1" className="bg-light-green text-dark">
                                <Form.Check
                                    onChange={e => setCurrentExp(e.target.checked)}
                                    type={"checkbox"}/>
                            </InputGroup.Text>
                            <Form.Control
                                disabled={true}
                                type="text"
                                placeholder="Current"
                            />
                        </InputGroup>
                        <InputGroup className="mb-2">
                            <InputGroup.Text id="basic-addon1" className="bg-light-green text-dark">To
                                Date</InputGroup.Text>
                            <Form.Control
                                disabled={currentExp}
                                type="date"
                                placeholder="To Date"
                            />
                        </InputGroup>
                        <InputGroup className="mb-2">
                            <InputGroup.Text id="basic-addon1"
                                             className="bg-light-green text-dark">Description</InputGroup.Text>
                            <Form.Control
                                as="textarea" rows={3}
                                placeholder="Description"
                            />
                        </InputGroup>
                        <Button variant="success" type="submit" className="me-1">
                            Update
                        </Button>
                        <Link to={'/profiles/dashboard'}>
                            <Button variant="dark" type="button">
                                Cancel
                            </Button>
                        </Link>
                    </Col>
                </Row>
            </Container>
        </>
    )
};
export default EditEducation;