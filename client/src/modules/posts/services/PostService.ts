import {ProfileView} from "../../profiles/models/ProfileView";
import axios from "axios";
import {IComment, PostView} from "../models/PostView";

export class PostService {
    private static serverUrl = process.env.REACT_APP_EXPRESS_SERVER;
    private static postsAPI: string = `${this.serverUrl}/api/posts`

    // create Post
    public static createPost(post: PostView, imageFile: any) {
        const formData = new FormData();
        formData.append('image', post.image);
        formData.append('text', post.text);
        formData.append('imageFile', imageFile);
       return axios.post(`${this.postsAPI}/`, formData, {
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          })
        // return axios.post(`${this.postsAPI}/`, post);
    }

    // Get all posts
    public static getAllPosts() {
        return axios.get(`${this.postsAPI}/`);
    }

    // Get a post with Post Id
    public static getPost(postId: string) {
        return axios.get(`${this.postsAPI}/${postId}`);
    }

    // Delete a post with Post Id
    public static deletePost(postId: string) {
        return axios.delete(`${this.postsAPI}/${postId}`);
    }

    // Like A Post with PostId
    public static likePost(postId: string) {
        return axios.post(`${this.postsAPI}/like/${postId}`);
    }

    // Un-Like A Post with PostId
    public static unLikePost(postId: string) {
        return axios.put(`${this.postsAPI}/unlike/${postId}`);
    }

    // Create Comment to a post
    public static createComment(postId: string, comment: IComment) {
        return axios.post(`${this.postsAPI}/comment/${postId}`, comment);
    }

    // DELETE Comment of a post
    public static deleteComment(commentId: string, postId: string) {
        return axios.delete(`${this.postsAPI}/comment/${postId}/${commentId}`);
    }
}