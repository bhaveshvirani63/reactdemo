import React, {useEffect, useState} from 'react';
import {Button, Card, Col, Container, Form, InputGroup, Row,} from "react-bootstrap";
import NavBarSocial from "../../../layout/components/navbar/NavBarSocial";
import {ToastUtil} from "../../../../util/ToastUtil";
import {IComment, PostView} from "../../models/PostView";
import * as postActions from '../../../../redux/posts/post.actions';
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch} from "../../../../redux/store";
import {postsFeatureKey, RootPostState} from "../../../../redux/posts/post.slice";
import Spinner from "../../../layout/components/spinner/Spinner";
import {RootUserState, usersFeatureKey} from "../../../../redux/users/user.slice";

interface IProps {
}

let PostList: React.FC<IProps> = ({}) => {
    const [selectedFile, setSelectedFile] = useState(null);
    const [comment, setComment] = useState<IComment>({
        text: "",
        _id: ""
    });

    // get posts data from redux
    const postState = useSelector((state: RootPostState) => {
        return state[postsFeatureKey];
    });

    // user state from redux
    const userState = useSelector((state: RootUserState) => {
        return state[usersFeatureKey];
    });

    const {loading, posts, post: reduxPost} = postState;
    const {user} = userState;

    const [validated, setValidated] = useState(false);
    const [validatedComment, setValidatedComment] = useState(false);
    let [showComments, setShowComments] = useState<boolean>(false);
    const dispatch: AppDispatch = useDispatch();

    const [post, setPost] = useState<PostView>({
        text: "",
        image: ""

    });

    const updateInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPost({
            ...post,
            [event.target.name]: event.target.value
        });
    };

    const handleSubmit = (event: any) => {
        event.preventDefault();
        const form = event.currentTarget;
        if (form.checkValidity() === true) {
            console.log('ddsdsds: ',post)
 
            dispatch(postActions.createPostAction({post: post, imageFile:selectedFile})).then((response: any) => {
                if (response.error) {
                    ToastUtil.displayErrorToast(response.error.message);
                } else {
                    ToastUtil.displaySuccessToast('Post is Created!');
                    setPost({
                        text: "",
                        image: ""
                    })
                }
            })
        }
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        setValidated(true);
    };

    useEffect(() => {
        dispatch(postActions.getAllPostsAction());
    }, []);

    const clickLikePost = (postId: string | undefined) => {
        console.log('clickLikePost:', postId)
        if (postId) {
            dispatch(postActions.likePostAction(postId)).then((response: any) => {
                if (response.error) {
                    ToastUtil.displayErrorToast("You had already liked the post!")
                }
            });
        }
    };

    const clickDisLikePost = (postId: string | undefined) => {
        console.log('clickDisLikePost:', postId)

        if (postId) {
            dispatch(postActions.unLikePostAction(postId)).then((response: any) => {
                if (response.error) {
                    ToastUtil.displayErrorToast("You had not liked the post already!")
                }
            });
        }
    };

    const handleAddComment = (event: any, postId: string | undefined) => {
        if (postId) {
            event.preventDefault();
            const form = event.currentTarget;
            if (form.checkValidity() === true) {
                dispatch(postActions.createCommentAction({postId: postId, comment: comment})).then((response: any) => {
                    if (response.error) {
                        ToastUtil.displayErrorToast(response.error.message);
                    } else {
                        ToastUtil.displaySuccessToast('Comment is Created!');
                        setComment({
                            text: "",
                            _id: ""
                        })
                    }
                })
            }
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }
            setValidatedComment(true);
        }
    };

    const clickDeleteComment = (commentId: string | undefined, postId: string | undefined) => {
        if (commentId && postId) {
            dispatch(postActions.deleteCommentAction({
                commentId: commentId,
                postId: postId
            })).then((response: any) => {
                if (response.error) {
                    ToastUtil.displayErrorToast(response.error.message);
                } else {
                    ToastUtil.displaySuccessToast('Comment is Deleted!');
                }
            });
        }
    };

    const clickDeletePost = (postId: string | undefined) => {
        if (postId) {
            dispatch(postActions.deletePostAction(postId)).then((response: any) => {
                if (response.error) {
                    ToastUtil.displayErrorToast(response.error.message);
                } else {
                    ToastUtil.displaySuccessToast('Post is Deleted!');
                }
            });
        }
    };

    const handleFileChange = (event:any) => {
        const selectedFile1 = event.target.files[0];
        console.log(`post: ${post}`);

        if (selectedFile1) {
            console.log(`Selected file: ${selectedFile1.name}`);
            console.log(`Selected file: ${selectedFile1.type}`);
             setSelectedFile(selectedFile1)
            // setPost({
            //     ...post,
            //     imageFile: selectedFile
            // });
        }
      };

    return (
        <>
            <NavBarSocial/>
            {
                loading && <Spinner/>
            }
            <Container className="mt-3">
                <Row>
                    <Col>
                        <h3 className="text-success">
                            <i className="fa fa-facebook-official"></i> React Social Community
                        </h3>
                        <p className="fst-italic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium
                            ad adipisci, assumenda atque deleniti deserunt dolorem doloribus maxime neque, odit placeat
                            quas quibusdam quidem quis quisquam quos reprehenderit similique ut!</p>
                    </Col>
                </Row>
            </Container>
            <Container>
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Row className="align-items-center shadow-lg p-2 my-3">
                        <Col xs={1}>
                            {
                                Object.keys(user).length > 0 &&
                                <img src={user.avatar}
                                     alt="" width={100} height={100} className="rounded-circle shadow-lg"/>
                            }
                        </Col>
                        <Col xs={10}>
                            <Form.Group className="mb-1">
                            <input
                            type="file"
                            className="form-control-file"
                            id="fileInput"
                                onChange={handleFileChange}
                                />
                                <Form.Control
                                    name={'image'}
                                    value={post.image}
                                    onChange={updateInput}
                                    type={'url'} placeholder={'Image Url'} required/>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid ImageURL.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-1">
                                <Form.Control
                                    name={'text'}
                                    value={post.text}
                                    onChange={updateInput}
                                    as="textarea" rows={3} placeholder="Whats on your mind?" required/>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Comment.
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Col>
                        <Col xs={1}>
                            <Button variant={'success'} type={'submit'} className="p-4 shadow-lg">Post</Button>
                        </Col>
                    </Row>
                </Form>
            </Container>

            <Container>
                <Row>
                    <Col>
                        {
                            posts.map(post => {
                                return (
                                    <Card key={post.post_id} className="mt-3">
                                        <Card.Body>
                                            <Row className="align-items-start">
                                                <Col xs={2} className="text-center align-self-start mt-5">
                                                    <img
                                                        src={post.avatar}
                                                        alt="" width={100} height={100}/><br/>
                                                    <small>{post.name}</small> <br/>
                                                    {
                                                        post && post.user && post.user.toString() === user._id &&
                                                        <Button variant='danger' className="mt-2"
                                                                onClick={() => clickDeletePost(post.post_id)}>
                                                            <i className="fa fa-trash-o"></i> Delete Post
                                                        </Button>
                                                    }
                                                </Col>
                                                <Col xs={5} className="align-self-start ">
                                                    <img
                                                        src={'https://chilly-aliens-love.loca.lt/uploads/'+ post.image}
                                                        alt="" className="img-fluid"/>
                                                </Col>
                                                <Col xs={5} className="align-self-start ">
                                                    <p>{post.text}</p>

                                                    <Row>
                                                        <Col className="d-flex">
                                                            <div className="text-center m-2">
                                                                <Button variant={'success'}
                                                                        onClick={() => clickLikePost(post.post_id)}>
                                                                    <i className="fa fa-thumbs-up"></i>
                                                                </Button><br/>
                                                                <small>{post.likes?.length}</small>
                                                            </div>
                                                            <div className="text-center m-2">
                                                                <Button variant={'danger'}
                                                                        onClick={() => clickDisLikePost(post.post_id)}>
                                                                    <i className="fa fa-thumbs-down"></i>
                                                                </Button><br/>
                                                            </div>
                                                            <div className="text-center m-2">

                                                                {
                                                                    !showComments ?
                                                                        <Button variant={'success'} className="me-2"
                                                                                onClick={e => setShowComments(true)}>
                                                                            <i className="fa fa-check-circle"></i> Show
                                                                            Comments
                                                                        </Button> :
                                                                        <Button variant={'warning'}
                                                                                onClick={e => setShowComments(false)}>
                                                                            <i className="fa fa-times-circle"></i> Hide
                                                                            Comments
                                                                        </Button>
                                                                }
                                                                <br/>
                                                                <small>{post.comments?.length}</small>
                                                            </div>
                                                        </Col>
                                                    </Row>

                                                    {/* Comments */}

                                                    <Row>
                                                        <Col>
                                                            <Form noValidate validated={validatedComment}
                                                                  onSubmit={(event) => handleAddComment(event, post.post_id)}>
                                                                <InputGroup className="mb-3 align-items-start">
                                                                    <Form.Group>
                                                                        <Form.Control
                                                                            placeholder="Your Comment"
                                                                            value={post.post_id == comment._id ? comment.text : ""}
                                                                            onChange={e => setComment({text: e.target.value, _id:post.post_id})}
                                                                            required
                                                                        />
                                                                        <Form.Control.Feedback>
                                                                            Looks Good!
                                                                        </Form.Control.Feedback>
                                                                        <Form.Control.Feedback type="invalid">
                                                                            Please choose a valid Comment.
                                                                        </Form.Control.Feedback>
                                                                    </Form.Group>
                                                                    <Form.Group>
                                                                        <InputGroup.Text id="basic-addon2">
                                                                            <Button variant={'success'}
                                                                                    type={'submit'}>Comment</Button>
                                                                        </InputGroup.Text>
                                                                    </Form.Group>
                                                                </InputGroup>
                                                            </Form>
                                                        </Col>
                                                    </Row>

                                                    {
                                                        showComments &&
                                                        <Row>
                                                            <Col>
                                                                {
                                                                    Object.keys(user).length > 0 && post.comments && post.comments?.map(comment => {
                                                                        return (
                                                                            <Card className="developer-card mt-3"
                                                                                  key={comment._id}>
                                                                                <Card.Body>
                                                                                    <Row>
                                                                                        <Col xs={10}>
                                                                                            <p>{comment.text}</p>
                                                                                            <small>By : <span
                                                                                                className="fw-bold">{comment.name}</span></small>
                                                                                            <br/>
                                                                                            <small>Date : <span
                                                                                                className="fw-bold"> {comment.date}</span></small>
                                                                                        </Col>
                                                                                        <Col xs={2}>
                                                                                            {
                                                                                                comment && comment.user && comment?.user.toString() === user._id &&
                                                                                                <Button variant='danger'
                                                                                                        onClick={() => clickDeleteComment(comment._id, post.post_id)}>
                                                                                                    <i className="fa fa-trash-o"></i>
                                                                                                </Button>
                                                                                            }
                                                                                        </Col>
                                                                                    </Row>
                                                                                </Card.Body>
                                                                            </Card>
                                                                        )
                                                                    })
                                                                }
                                                            </Col>
                                                        </Row>
                                                    }
                                                </Col>
                                            </Row>
                                        </Card.Body>
                                    </Card>
                                )
                            })
                        }
                    </Col>
                </Row>
            </Container>
        </>
    )
};
export default PostList;