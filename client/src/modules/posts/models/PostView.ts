export interface PostView {
    post_id?: string;
    user?: string;
    text: string;
    image: string;
    name?: string;
    avatar?: string;
    likes?: ILike[],
    comments?: IComment[],
    createdAt?: string;
    updatedAt?: string;
    imageFile?: File
}

export interface ILike {
    _id?: string;
    user?: string;
}

export interface IComment {
    _id?: string;
    user?: string;
    text: string;
    name?: string;
    avatar?: string;
    date?: string;
}

