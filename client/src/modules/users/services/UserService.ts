import {UserView} from "../models/UserView";
import axios from 'axios';

export class UserService {
    private static serverUrl = process.env.REACT_APP_EXPRESS_SERVER;

    private static usersAPI: string = `${UserService.serverUrl}/api/users`

    // public
    public static registerUser(user: UserView) {
        return axios.post(`${this.usersAPI}/register`, user);
    }

    // public
    public static loginUser(user: UserView) {
        return axios.post(`${this.usersAPI}/login`, user);
    }

    // private url
    public static getUserInfo() {
        return axios.get(`${this.usersAPI}/me`);
    }
}