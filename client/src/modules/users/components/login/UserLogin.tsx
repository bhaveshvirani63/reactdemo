import React, {useState} from 'react';
import {Button, Col, Container, Form, Row} from "react-bootstrap";
import {Link, useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {RootUserState, usersFeatureKey} from "../../../../redux/users/user.slice";
import {AppDispatch} from "../../../../redux/store";
import {UserView} from "../../models/UserView";
import * as userActions from "../../../../redux/users/user.actions";
import {ToastUtil} from "../../../../util/ToastUtil";
import Spinner from "../../../layout/components/spinner/Spinner";
import NavBarSocial from "../../../layout/components/navbar/NavBarSocial";

interface IProps {
}

let UserLogin: React.FC<IProps> = ({}) => {

    const userState = useSelector((state: RootUserState) => {
        return state[usersFeatureKey];
    });

    let {loading} = userState;

    const dispatch: AppDispatch = useDispatch();
    const navigate = useNavigate();

    const [validated, setValidated] = useState(false);

    const [user, setUser] = useState<UserView>({
        email: "",
        password: ""
    });

    const updateInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUser((prevState) => {
            return {
                ...prevState,
                [event.target.name]: event.target.value
            }
        })
    };


    const handleSubmit = (event: any) => {
        event.preventDefault();
        const form = event.currentTarget;
        if (form.checkValidity() === true) {
            dispatch(userActions.loginUserAction(user)).then((response: any) => {
                if (response.error) {
                    ToastUtil.displayErrorToast(response.error.message);
                } else {
                    ToastUtil.displaySuccessToast('Login is Success!');
                    navigate('/profiles/dashboard');
                }
            })
        }
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        setValidated(true);
    };

    return (
        <>
            <NavBarSocial/>
            {
                loading && <Spinner/>
            }
            <Container>
                <Row>
                    <Col>
                        <h3 className="mt-3 text-success">
                            <i className="fa fa-sign-in"></i> Login</h3>
                        <p className="fst-italic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A
                            consectetur est iure officiis praesentium quod, sunt. Accusamus, cumque eligendi in incidunt
                            ipsum possimus provident recusandae reprehenderit rerum veritatis. Consectetur, itaque.</p>
                    </Col>
                </Row>
                <Row>
                    <Col xs={4}>
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                            <Form.Group className="mb-3">
                                <Form.Control
                                    name={'email'}
                                    value={user.email}
                                    onChange={updateInput}
                                    type="email" placeholder="Email" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Email.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Control
                                    name={'password'}
                                    value={user.password}
                                    onChange={updateInput}
                                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{5,10}$"
                                    type="password" placeholder="Password" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a Strong Password.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Button variant="success" type="submit">
                                Login
                            </Button>
                        </Form>
                        <small className="mt-2">Don't have an Account ?
                            <Link to={'/users/register'}
                                  className="text-decoration-none text-success fw-bold"> Register</Link>
                        </small>
                    </Col>
                </Row>
            </Container>
        </>
    )
};
export default UserLogin;