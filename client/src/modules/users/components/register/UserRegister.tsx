import React, {useState} from 'react';
import {Button, Col, Container, Form, Row} from "react-bootstrap";
import {Link, useNavigate} from "react-router-dom";
import {UserView} from "../../models/UserView";
import {useDispatch, useSelector} from "react-redux";
import {AppDispatch} from "../../../../redux/store";
import * as userActions from '../../../../redux/users/user.actions';
import {RootUserState, usersFeatureKey} from "../../../../redux/users/user.slice";
import Spinner from "../../../layout/components/spinner/Spinner";
import {ToastUtil} from "../../../../util/ToastUtil";
import NavBarSocial from "../../../layout/components/navbar/NavBarSocial";

interface IProps {
}

let UserRegister: React.FC<IProps> = ({}) => {

    const userState = useSelector((state: RootUserState) => {
        return state[usersFeatureKey];
    });

    let {loading} = userState;

    const dispatch: AppDispatch = useDispatch();
    const navigate = useNavigate();

    const [validated, setValidated] = useState(false);

    const [user, setUser] = useState<UserView>({
        name: "",
        email: "",
        password: ""
    });

    const updateInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUser((prevState) => {
            return {
                ...prevState,
                [event.target.name]: event.target.value
            }
        })
    };


    const handleSubmit = (event: any) => {
        event.preventDefault();
        const form = event.currentTarget;
        if (form.checkValidity() === true) {
            dispatch(userActions.registerUserAction(user)).then((response: any) => {
                if (response.error) {
                    ToastUtil.displayErrorToast(response.error.message);
                } else {
                    ToastUtil.displaySuccessToast('Registration is Success!');
                    navigate('/users/login');
                }
            })
        }
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        setValidated(true);
    };

    return (
        <>
            <NavBarSocial/>
            {
                loading && <Spinner/>
            }

            <Container>
                <Row>
                    <Col>
                        <h3 className="mt-3 text-success">
                            <i className="fa fa-user-secret"></i> Registration</h3>
                        <p className="fst-italic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet
                            beatae dignissimos dolor dolorem eaque eligendi eum expedita, facilis harum impedit iusto
                            neque non praesentium quae quidem sapiente temporibus voluptatibus.</p>
                    </Col>
                </Row>
                <Row>
                    <Col xs={4}>
                        <Form noValidate validated={validated} onSubmit={handleSubmit}>
                            <Form.Group className="mb-3">
                                <Form.Control
                                    name={'name'}
                                    value={user.name}
                                    onChange={updateInput}
                                    pattern="[a-zA-Z0-9]{4,10}"
                                    type="text" placeholder="Username" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Username.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Control
                                    name={'email'}
                                    value={user.email}
                                    onChange={updateInput}
                                    type="email" placeholder="Email" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a valid Email.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="mb-3">
                                <Form.Control
                                    name={'password'}
                                    value={user.password}
                                    onChange={updateInput}
                                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{5,10}$"
                                    type="password" placeholder="Password" required></Form.Control>
                                <Form.Control.Feedback>
                                    Looks Good!
                                </Form.Control.Feedback>
                                <Form.Control.Feedback type="invalid">
                                    Please choose a Strong Password.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Button variant="success" type="submit">
                                Register
                            </Button>
                        </Form>
                        <small className="mt-2">Already have an Account ?
                            <Link to={'/users/login'}
                                  className="text-decoration-none text-success fw-bold"> Login</Link>
                        </small>
                    </Col>
                </Row>
            </Container>
        </>
    )
};
export default UserRegister;