import React from 'react';
import {Container, Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {logOutAction, RootUserState, usersFeatureKey} from "../../../../redux/users/user.slice";
import {AuthUtil} from "../../../../util/AuthUtil";
import {AppDispatch} from "../../../../redux/store";


interface IProps {
}

let NavBarSocial: React.FC<IProps> = ({}) => {
    const dispatch: AppDispatch = useDispatch();

    const userState = useSelector((state: RootUserState) => {
        return state[usersFeatureKey];
    });

    const {isAuthenticated, user} = userState;

    const clickLogOut = () => {
        dispatch(logOutAction({}));
    };

    return (
        <>
            <Navbar bg="dark" expand="sm" variant="dark">
                <Container>
                    <Navbar.Brand>
                        <Link to={'/'} className="text-decoration-none text-light">
                            <i className="fa fa-github text-success"></i> React Social</Link>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link>
                                <Link to={'/developers'} className="text-decoration-none text-light">
                                    <i className="fa fa-user-circle text-muted"></i> Developers</Link>
                            </Nav.Link>
                            {
                                AuthUtil.isLoggedIn() &&
                                <>
                                    <Nav.Link>
                                        <Link to={'/posts/all'}
                                              className="text-decoration-none text-light">
                                            <i className="fa fa-github text-muted"></i> Community</Link>
                                    </Nav.Link>
                                    <Nav.Link>
                                        <Link to={'/profiles/dashboard'}
                                              className="text-decoration-none text-light">
                                            <i className="fa fa-dashboard text-muted"></i> Dashboard</Link>
                                    </Nav.Link>
                                </>
                            }
                        </Nav>
                        <Nav className="ms-auto">

                            {
                                AuthUtil.isLoggedIn() &&
                                <>
                                    {
                                        Object.keys(user).length > 0 &&
                                        <Nav.Link>
                                            <Link to={'/profiles/dashboard'}
                                                  className="text-decoration-none text-light">
                                                <img src={user.avatar} alt="" width={20}
                                                     className="rounded-circle shadow-lg"/>{' '}
                                                {user.name}</Link>
                                        </Nav.Link>
                                    }
                                    <Nav.Link>
                                        <Link to={'/profiles/dashboard'}
                                              className="text-decoration-none text-light"
                                              onClick={clickLogOut}>
                                            <i className="fa fa-sign-out text-muted"></i> LogOut</Link>
                                    </Nav.Link>
                                </>
                            }
                            {
                                !AuthUtil.isLoggedIn() &&
                                <>
                                    <Nav.Link>
                                        <Link to={'/users/login'}
                                              className="text-decoration-none text-light">
                                            <i className="fa fa-sign-in text-muted"></i> Login</Link>
                                    </Nav.Link>
                                </>
                            }
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
};
export default NavBarSocial;