import React from 'react';
import NavBarSocial from "../navbar/NavBarSocial";

interface IProps {
}

let About: React.FC<IProps> = ({}) => {
    return (
        <>
            <NavBarSocial/>
            <h3>About Component</h3>
        </>
    )
};
export default About;