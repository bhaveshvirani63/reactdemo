import React from 'react';
import {Button} from "react-bootstrap";
import {Link} from "react-router-dom";

interface IProps {
}

let Home: React.FC<IProps> = ({}) => {
    return (
        <>
            <div className="landing">
                <div className="wrapper">
                    <div className="d-flex flex-column justify-content-center align-items-center h-100 text-center">
                        <h3 className="display-2">
                            <i className="fa fa-github text-success"></i> React Social</h3>
                        <p className="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium
                            aliquam amet architecto distinctio, eos error ex fugiat fugit harum <br/> ipsum labore minus
                            modi
                            nostrum provident quisquam reprehenderit suscipit veritatis voluptatem.</p>
                        <div>
                            <Button variant={'secondary'} className="me-2">
                                <Link to={'/developers'}
                                      className="text-decoration-none text-white">Developers</Link>
                            </Button>
                            <Button variant={'success'}>
                                <Link to={'/users/login'} className="text-decoration-none text-white">Login</Link>
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
};
export default Home;