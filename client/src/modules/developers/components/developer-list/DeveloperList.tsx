import React, {useEffect} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import DeveloperCard from "../developer-card/DeveloperCard";
import NavBarSocial from "../../../layout/components/navbar/NavBarSocial";
import {useDispatch, useSelector} from "react-redux";
import * as developerActions from '../../../../redux/developers/developer.actions';
import {AppDispatch} from "../../../../redux/store";
import {developersFeatureKey, RootDeveloperState} from "../../../../redux/developers/developer.slice";
import Spinner from "../../../layout/components/spinner/Spinner";

interface IProps {
}

let DeveloperList: React.FC<IProps> = ({}) => {

    const dispatch: AppDispatch = useDispatch();

    // get dev data from redux
    const developerState = useSelector((state: RootDeveloperState) => {
        return state[developersFeatureKey];
    });

    const {loading, developers} = developerState;

    useEffect(() => {
        dispatch(developerActions.getAllDevelopersAction());
    }, [])


    return (
        <>
            <NavBarSocial/>
            {
                loading && <Spinner/>
            }
            <Container>
                <Row>
                    <Col>
                        <h3 className="text-success">
                            <i className="fa fa-user-circle mt-3 text-success"></i> Developers</h3>
                        <p className="fst-italic">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Exercitationem maiores minima nostrum similique! Debitis eligendi eum nobis totam. Ab
                            accusamus animi earum est explicabo laborum nobis praesentium qui temporibus voluptates?</p>
                    </Col>
                </Row>
                <DeveloperCard developers={developers}/>
            </Container>
        </>
    )
};
export default DeveloperList;