import React from 'react';
import {Badge, Card, Col, Container, Row} from "react-bootstrap";
import {DeveloperView} from "../../models/DeveloperView";
import {Link} from "react-router-dom";

interface IProps {
    developer: DeveloperView
}

let DeveloperDetailsCard: React.FC<IProps> = ({developer}) => {
    return (
        <>
            <Row>
                <Col>
                    <Card className="shadow-lg developer-card text-center">
                        <Card.Body>
                            <Row>
                                <Col xs={2} className="offset-5">
                                    <img src={developer.user.avatar}
                                         alt="" className="img-fluid rounded-circle shadow-lg"/>
                                </Col>
                            </Row>
                            <Row className="text-center mt-3">
                                <Col>
                                    <h3>{developer.user.name}</h3>
                                    <h5>{developer.company}</h5>
                                    <p>{developer.location}</p>
                                    <a className="text-white text-decoration-none"
                                       href={`https://github.com/${developer.githubUsername}`} target="_blank">GitHub
                                        Profile</a>
                                    <div>
                                        <a href={developer.social.facebook} target="_blank">
                                            <i className="fa fa-facebook-square m-3 text-primary"></i>
                                        </a>
                                        <a href={developer.social.twitter} target="_blank">
                                            <i className="fa fa-twitter-square m-3 text-primary"></i>
                                        </a>
                                        <a href={developer.social.linkedin} target="_blank">
                                            <i className="fa fa-linkedin-square m-3 text-primary"></i>
                                        </a>
                                        <a href={developer.social.instagram} target="_blank">
                                            <i className="fa fa-instagram m-3 text-warning"></i>
                                        </a>
                                        <a href={developer.social.youtube} target="_blank">
                                            <i className="fa fa-youtube-square m-3 text-danger"></i>
                                        </a>
                                    </div>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>

            <Row className="mt-3 text-center">
                <Col>
                    <Card>
                        <Card.Body className="p-4 shadow-sm">
                            <h3 className="text-success">{developer.user.name}'s Biography</h3>
                            <p>{developer.bio}</p>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>

            <Row className="mt-3 text-center">
                <Col>
                    <Card>
                        <Card.Body className="p-4 shadow-sm">
                            <h3 className="text-success">Skill Set</h3>
                            {
                                developer.skills.map((skill) => {
                                    return (
                                        <Badge key={skill} bg="success" pill className="m-3">
                                            <i className="fa fa-check-circle"></i> {skill}
                                        </Badge>
                                    )
                                })
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>

            <Row className="mt-3">
                <Col xs={6}>

                    <Card>
                        <Card.Header className="bg-light p-3 text-success">
                            <h3>Experience Details</h3>
                        </Card.Header>
                        <Card.Body>
                            <Card className="developer-card">
                                <ul className="list-group">
                                    {
                                        developer.experience.map((exp) => {
                                            return (
                                                <li className="list-group-item m-3">
                                                    <span>Title : <span
                                                        className="fw-bold">{exp.title}</span></span><br/>
                                                    <span>Company : <span
                                                        className="fw-bold">{exp.company}</span></span><br/>
                                                    <span>Location : <span
                                                        className="fw-bold">{exp.location}</span></span><br/>
                                                    <span>Description : <span
                                                        className="fw-bold">{exp.description}</span></span><br/>
                                                    <span>From Date : <span
                                                        className="fw-bold">{exp.from}</span></span><br/>
                                                    <span>To Date : <span
                                                        className="fw-bold">{exp.to}</span></span><br/>
                                                    <span>Current Exp : <span
                                                        className="fw-bold">{exp.current.toString()}</span></span><br/>
                                                </li>
                                            )
                                        })
                                    }
                                </ul>
                            </Card>
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={6}>
                    <Card>
                        <Card.Header className="bg-light p-3 text-success">
                            <h3>Education Details</h3>
                        </Card.Header>
                        <Card.Body>
                            <Card className="developer-card">
                                <ul className="list-group">
                                    {
                                        developer.education.map((edu) => {
                                            return (
                                                <li className="list-group-item m-3">
                                                    <span>School : <span
                                                        className="fw-bold">{edu.school}</span></span><br/>
                                                    <span>Degree : <span
                                                        className="fw-bold">{edu.degree}</span></span><br/>
                                                    <span>Field Of Study : <span
                                                        className="fw-bold">{edu.fieldOfStudy}</span></span><br/>
                                                    <span>Description : <span
                                                        className="fw-bold">{edu.description}</span></span><br/>
                                                    <span>From Date : <span
                                                        className="fw-bold">{edu.from}</span></span><br/>
                                                    <span>To Date : <span
                                                        className="fw-bold">{edu.to}</span></span><br/>
                                                    <span>Current Exp : <span
                                                        className="fw-bold">{edu.current.toString()}</span></span><br/>
                                                </li>
                                            )
                                        })
                                    }
                                </ul>
                            </Card>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <div style={{marginBottom: '100px'}}></div>
        </>
    )
};
export default DeveloperDetailsCard;