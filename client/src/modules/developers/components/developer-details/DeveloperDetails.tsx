import React, {useEffect} from 'react';
import {Col, Container, Row} from "react-bootstrap";
import DeveloperDetailsCard from "./DeveloperDetailsCard";
import NavBarSocial from "../../../layout/components/navbar/NavBarSocial";
import {useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import * as developerActions from '../../../../redux/developers/developer.actions';
import {AppDispatch} from "../../../../redux/store";
import {developersFeatureKey, RootDeveloperState} from "../../../../redux/developers/developer.slice";
import Spinner from "../../../layout/components/spinner/Spinner";

interface IProps {
}

let DeveloperDetails: React.FC<IProps> = ({}) => {
    const {developerId} = useParams();
    const dispatch: AppDispatch = useDispatch();

    // get dev data from redux
    const developerState = useSelector((state: RootDeveloperState) => {
        return state[developersFeatureKey];
    });

    const {loading, developer} = developerState;

    useEffect(() => {
        if (developerId) {
            dispatch(developerActions.getDeveloperAction(developerId));
        }
    }, [developerId]);


    return (
        <>
            <NavBarSocial/>
            {
                loading && <Spinner/>
            }
            
            {
                Object.keys(developer).length > 0 &&
                <Container>
                    <Row>
                        <Col>
                            <h3 className="text-success mt-3">
                                <i className="fa fa-user-circle"></i> {developer.user.name}' Profile
                            </h3>
                            <p className="fst-italic">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias
                                architecto asperiores delectus dicta dignissimos dolorem ea eos error expedita impedit
                                ipsam
                                minima mollitia nesciunt, nihil nobis numquam odit provident soluta.</p>
                        </Col>
                    </Row>
                    <DeveloperDetailsCard developer={developer}/>
                </Container>
            }

        </>
    )
};
export default DeveloperDetails;