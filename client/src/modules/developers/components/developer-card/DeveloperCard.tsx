import React from 'react';
import {Badge, Button, Card, Col, Row} from "react-bootstrap";
import {Link} from "react-router-dom";
import {DeveloperView} from "../../models/DeveloperView";

interface IProps {
    developers: DeveloperView[]
}

let DeveloperCard: React.FC<IProps> = ({developers}) => {
    return (
        <>
            {
                developers.length > 0 && developers.map((developer) => {
                    return (
                        <Card className="shadow-lg developer-card" key={developer.user._id}>
                            <Card.Body>
                                <Row className="align-items-center">
                                    <Col xs={3}>
                                        <img src={developer.user.avatar}
                                             alt=""
                                             className="img-fluid"/>
                                    </Col>
                                    <Col xs={4} className="offset-1">
                                        <h3>Name : {developer.user.name}</h3>
                                        <span>Designation : {developer.designation}</span><br/>
                                        <span>Company : {developer.company}</span><br/>
                                        <span>Location : {developer.location}</span><br/>
                                        <span>
                                   <Link to={`/developers/${developer._id}`}>
                                   <Button variant={'success'}>View Profile</Button>
                               </Link>
                           </span>
                                    </Col>
                                    <Col xs={2}>
                           <span>
                               {
                                   developer.skills.map((skill) => {
                                       return (
                                           <div key={skill}>
                                               <Badge bg="success" pill>
                                                   <i className="fa fa-check-circle"></i> {skill}
                                               </Badge><br/>
                                           </div>
                                       )
                                   })
                               }
                           </span>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    )
                })
            }
        </>
    )
};
export default DeveloperCard;