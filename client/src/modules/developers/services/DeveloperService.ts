import axios from "axios";

export class DeveloperService {

    private static serverUrl = process.env.REACT_APP_EXPRESS_SERVER;

    private static developersAPI: string = `${this.serverUrl}/api/profiles`

    public static getAllDevelopers() {
        return axios.get(`${this.developersAPI}/all`);
    }

    public static getDeveloper(profileId: string) {
        return axios.get(`${this.developersAPI}/${profileId}`);
    }
}